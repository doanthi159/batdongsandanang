<!DOCTYPE html>
<html>
<head>
<title><?php echo $title; ?> by www.hoangcode.com</title>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

<style type="text/css">
.demo_ct{
  margin: 30px 0px;  
}
.rowpt a li{
    padding: 5px 0px;
}
.rowpt a{
    text-decoration: none;
    color: blue;
}
.paging {
  text-align: center;
  margin: 0px 0px 10px 0px;
  font: 12px 'Tahoma';
  margin-top: 30px;
  padding-bottom: 50px;
  border-bottom: 1px solid #e2e2e2;
}
.paging a:hover{
    color: white;
    font: 12px 'Tahoma';
    text-shadow:0px 1px #388DBE;
    border-color:#3390CA;
    background:#58B0E7;
    background:-moz-linear-gradient(top, #B4F6FF 1px, #63D0FE 1px, #58B0E7);
    background:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0.02, #B4F6FF), color-stop(0.02, #63D0FE), color-stop(1, #58B0E7));
    border-radius: 3px 3px 3px 3px;
    padding: 5px 10px;
}
.paging a {
    font: 12px 'Tahoma';
    color: #0A7EC5;
    border: solid 1px;
    border-color: #8DC5E6;
    background: #F8FCFF;
    border-radius: 3px 3px 3px 3px;
    padding: 5px 10px;
    margin: 0px 5px;
}
.paging strong {
    color: white;
    font: 12px 'Tahoma';
    text-shadow:0px 1px #388DBE;
    border-color:#3390CA;
    background:#58B0E7;
    background:-moz-linear-gradient(top, #B4F6FF 1px, #63D0FE 1px, #58B0E7);
    background:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0.02, #B4F6FF), color-stop(0.02, #63D0FE), color-stop(1, #58B0E7));
    border-radius: 3px 3px 3px 3px;
    padding: 6px 12px;
}
</style>          


</head>
<body>
    <div class="header-wrapper">
        <div class="ct-wrapper">
            <div class="home_demo">
                
                <!-- demo -->             
                <div class="content_wrapper">
                  
                    <div class="demo_ct">     
                        <ul class="rowpt">              
                        <?php foreach($post->result() as $row): ?>     
                            <a href="<?php echo $row->idbaiviet; ?>" title="<?php echo $row->idbaiviet; ?>"><li><?php echo $row->tenbaiviet; ?></li></a>  
                        <?php endforeach;?>
                        </ul>  
                    <div class="paging"><?php echo $paginator; ?></div>                                       
                    </div> 
                        
            </div>
        </div>
    </div>
</body>
</html>