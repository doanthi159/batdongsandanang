
  <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="../teamplate/USER/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../teamplate/USER/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="../teamplate/USER/bootstrap/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<div id="main-body">
    <section class="breadcrumbs">
        <div class="container">
            <div class="content">
                <span class="item"><a href="?page=trangchu" title="Trang chủ">Trang cá nhân</a></span>
                <span class="item_sep"> / </span>
                <span class="item"><a href="?page=bantin" title="Tin Thị trường">Bài viết đã đăng</a></span>
            </div>
        </div>
    </section>
    <div class="container">
        <?php  if(isset($_GET['thongbao'])){ ?>
            <div class="notification success png_bg">
                <a href="#" class="close"><img src="teamplate/ADMIN/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close"></a>
                <div>
                    <?php echo $_GET['thongbao']; ?>
                </div>
            </div>
            <?php } ?>

            <?php 
                if(isset($_GET['error'])){
            ?>
            <div class="notification error png_bg">
                <a href="#" class="close"><img src="teamplate/ADMIN/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close"></a>
                <div>
                    <?php echo $_GET['error']; ?>
                </div>
            </div>
            <?php } ?>
         <table class="table table-bordered" id="dataTable">
            <thead>
                <tr><th>Id bài viết</th>
                    <th>Tên bài viết</th>
                    <th>Ảnh minh họa</th>
                    <th>Nội dung tóm tắt</th>
                    <th>Trạng thái</th>
                    <th>Chức năng</th>
                </tr>
            </thead>
            <tbody>
                <?php if (isset($baiviet) && count($baiviet) > 0) {
                    foreach ($baiviet as $key => $value) {
                       ?>

                <tr>
                    <td><?php echo $value['idbaiviet']; ?></td>
                    <td><?php echo $value['tenbaiviet']; ?></td>
                    <td width="15%"><img src="teamplate/USER/img/<?php echo  $value['anhminhhoa'] ?>" style="width:50px;height: 50px" /></td>
                    <td width="40%"><?php echo $value['tomtat']; ?></td>
                    <td width="10%">
                        <!-- Icons -->
                        <?php if($value['trangthai']==0){ ?>
                        <a href="?page=baivietsua&id=<?php echo $value['idbaiviet'] ?>" title="Phê duyệt"><img style="width:23px;height:23px;" src="teamplate/ADMIN/resources/images/icons/verify.png" alt="Edit" /></a>
                        <?php }else{ ?>
                            <img style="width:23px;height:23px;" title="Đã phê duyệt" src="teamplate/ADMIN/resources/images/icons/verifyd.png" alt="Edit" disabled />
                            <?php } ?>
                    </td>
                    <td><a href="admin/delete_bv?id=<?php echo $value['idbaiviet'] ?>" title="Sửa"><span class="glyphicon glyphicon-pencil" style="font-size: 27px;"></span></a>
                        <a href="user/delete_bv?id=<?php echo $value['idbaiviet'] ?>" title="Xóa"><img src="teamplate/ADMIN/resources/images/icons/cross.png" alt="Delete" onclick="return confirm('Bạn có chắc xóa không?');"/></a> </td>
                </tr>
                <?php
                    }
                } ?>

            </tbody>
        </table>
    </div>
   
</div>
<script src="" type="text/javascript" charset="utf-8" async defer>
    $(document).ready( function () {
    $('#dataTable').DataTable();
} );
</script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript" charset="utf-8" async defer></script>