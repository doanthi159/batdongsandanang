<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>Đất Xanh Bắc Miền Trung | batdongsandanang.com</title>
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Đất Xanh Bắc Miền Trung | datxanhbacmientrung.com" />
    <meta property="og:url" content="http://batdongsandanang.com" />
    <meta property="og:image:url" content="frontend/images/img_fb.jpg" />
    <meta property="og:image" content="frontend/images/img_fb.jpg" />
    <meta property="og:site_name" content="Đất Xanh Bắc Miền Trung | datxanhbacmientrung.com" />
    <meta property="og:description" content="Website chính thức của Công ty CP Đất Xanh Bắc Miền Trung. Chúng tôi luôn cung cấp cho khách hàng giải pháp nhà đất hiệu quả nhất, dịch vụ tốt nhất, giá cả phù hợp nhất!" />
    <meta name="keywords" content="Đất Xanh, Tập đoàn Đất Xanh, Đất Xanh Bắc Miền Trung, Bất động sản, Căn hộ đất xanh, công ty Đất Xanh, căn hộ cao cấp, dự án cao cấp">
    </meta>
    <meta name="robots" content="index,follow" />
    <meta name="description" content="Website chính thức của Công ty CP Đất Xanh Bắc Miền Trung. Chúng tôi luôn cung cấp cho khách hàng giải pháp nhà đất hiệu quả nhất, dịch vụ tốt nhất, giá cả phù hợp nhất!">
    </meta>
    <meta name="alexa" content="100">
    <meta name="revisit" content="2 days">
    <meta name="revisit-after" content="2 days">
    <meta name="copyright" content="batdongsandanang.com" />
    <meta name="author" content="batdongsandanang.com" />
    <base target='_self' />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" type="text/css" href="logo.png" />
    <link rel="stylesheet" type="text/css" href="teamplate/USER/library/slick/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="teamplate/USER/library/fancybox-master/dist/jquery.fancybox.css" />
    <link rel="stylesheet" type="text/css" href="teamplate/USER/library/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="teamplate/USER/library/font-awesome-4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="teamplate/USER/library/aos/aos.css" />
    <link rel="stylesheet" type="text/css" href="teamplate/USER/frontend/css/style.css" />
    <script type="text/javascript">
        var url = "http://batdongsandanang.com/";
    </script>
    <script type="text/javascript" src="teamplate/USER/library/jQuery/jquery.min.js"></script>
    <script type="text/javascript" src="teamplate/USER/library/fancybox-master/dist/jquery.fancybox.js"></script>
    <script type="text/javascript" src="teamplate/USER/library/aos/aos.js"></script>
    <script type="text/javascript" src="teamplate/USER/library/slick/slick/slick.js"></script>
    <script src="teamplate/ADMIN/ckeditor/ckeditor.js"></script>
</head>

<body>




    <div class="page">

        <!-- MENU -->

        <nav class="main_menu">
            <div class="container">
                <ul class="menunav">
                    <li class="active"><a href="?page=trangchu">Trang chủ</a></li>
                    <li class=""><a href="?page=gioithieu">Giới thiệu</a>
                       
                    </li>
                    <li class=""><a href="?page=duan">Dự án</a>
                        <ul class="level1">
                            <li class=""><a href="?page=duan&id=1&count=5"><i class="fa fa-angle-right" aria-hidden="true"></i>Nhà
                                    phố</a></li>
                            <li class=""><a href="?page=duan&id=2&count=5"><i class="fa fa-angle-right" aria-hidden="true"></i>Biệt
                                    thự</a></li>
                            <li class=""><a href="?page=duan&id=3&count=5"><i class="fa fa-angle-right" aria-hidden="true"></i>Đất
                                    và móng</a></li>
                            <li class=""><a href="?page=duan&id=4&count=5"><i class="fa fa-angle-right" aria-hidden="true"></i>Đất
                                    nền</a></li>
                        </ul>
                    </li>
                    <li class=""><a href="?page=tintuc&id=1&count=5">Tin tức</a>
                        <ul class="level1">
                            <li class=""><a href="?page=tintuc&id=1&count=5"><i class="fa fa-angle-right" aria-hidden="true"></i>Tin
                                    BDS Đà Nẵng</a></li>
                            <li class=""><a href="?page=tintuc&id=2&count=5"><i class="fa fa-angle-right" aria-hidden="true"></i>Tin
                                    thị
                                    trường</a></li>
                        </ul>
                    </li>
                </ul>
                <div class="logo">
                    <a href="?page=trangchu"><img src="teamplate/USER/frontend/images/logo_gif.gif" /></a>
                </div>
                <ul class="menunav">
                    <li class=""><a href="?page=uudai">Ưu đãi<i class="fa fa-gift" aria-hidden="true"></i></a></li>
                   
                    <li class=""><a hre="" onclick="checkstatus()">Đăng Tin</a></li>
                    <li class=""><a href="?page=lienhe">Liên hệ</a></li>
                    <li class="" id="IDdangxuat">
                        <?php if(isset($_SESSION['user']) ) { ?>
                        <a href="index.php/userLogout"><i class="fa fa-sign-out"></i></a>
                        <ul class="level1">
                            <li class=""><a href="index.php/user"></i>
                                    Tài khoảng cá nhân</a></li>
                            <li class=""><a href=""></i>
                                    Đổi mật khẩu</a></li>
                            <li class=""><a href="index.php/userLogout"></i>
                                    Đăng xuất</a></li>
                        </ul>
                        <?php } ?>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="rd-mobilemenu">
            <ul class="rd-mobilemenu_ul">

                <ul class="menunav">
                    <li class="active"><a href="?page=trangchu">Trang chủ</a></li>
                    <li class=""><a href="?page=gioithieu">Giới thiệu</a>
                       
                    </li>
                    <li class=""><a href="?page=duan">Dự án</a>
                        <ul class="level1">
                            <li class=""><a href="?page=duan&id=1&count=5"><i class="fa fa-angle-right" aria-hidden="true"></i>Nhà
                                    phố</a></li>
                            <li class=""><a href="?page=duan&id=2&count=5"><i class="fa fa-angle-right" aria-hidden="true"></i>Biệt
                                    thự</a></li>
                            <li class=""><a href="?page=duan&id=3&count=5"><i class="fa fa-angle-right" aria-hidden="true"></i>Đất
                                    và móng</a></li>
                            <li class=""><a href="?page=duan&id=4&count=5"><i class="fa fa-angle-right" aria-hidden="true"></i>Đất
                                    nền</a></li>
                        </ul>
                    </li>
                    <li class=""><a href="?page=tintuc&id=1&count=5">Tin tức</a>
                        <ul class="level1">
                            <li class=""><a href="?page=tintuc&id=1&count=5"><i class="fa fa-angle-right" aria-hidden="true"></i>Tin
                                    BDS Đà Nẵng</a></li>
                            <li class=""><a href="?page=tintuc&id=2&count=5"><i class="fa fa-angle-right" aria-hidden="true"></i>Tin
                                    thị
                                    trường</a></li>
                        </ul>
                    </li>
                </ul>
        </div>

        <div class="rd-mobilepanel">
            <button class="rd-mobilepanel_toggle"><span></span></button>
            <div class="rd-mobilepanel_title">Menu</div>
            <div class="logo">
                <a ref="http://batdongsandanang.com"><img src="teamplate/USER/frontend/images/logo_gif.gif" /></a>
            </div>
        </div>




        <script type="text/javascript">

            jQuery(document).ready(function () {

                $(window).scroll(function () {
                    if ($(window).scrollTop() > 0) {
                        $('nav.main_menu').addClass('stick');
                        $('.page').css('padding-top', '70px');
                    } else {
                        $('nav.main_menu').removeClass('stick');
                        $('.page').css('padding-top', '80px');
                    }

                    if ($(window).scrollTop() > 500) {
                        $('.box_action').css('visibility', 'visible');
                        $('.box_action .gotop').addClass('active');
                    } else {
                        $('.box_action').css('visibility', 'hidden');
                        $('.box_action .gotop').removeClass('active');
                    }
                });
            });

            // menu toggle
            $('button.rd-mobilepanel_toggle').click(function (event) {
                $(this).toggleClass("active");
                $(".rd-mobilemenu").toggleClass('active', $(this).hasClass('active'));
            });

            // Go top
            $('.box_action .gotop').click(function () {
                $('html,body').animate({ scrollTop: 0 }, 'duration');
            });

            var btn_loop = function () {
                setTimeout(function () {
                    $('.main_menu ul.menunav:last-child li:first-child a').toggleClass('ac');
                    btn_loop();
                }, 500);
            }
            btn_loop();
        </script>

        <div class="modal fade" id="modelLogin" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" onclick="delStatus()" class="close" data-dismiss="modal">&times;</button>

                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">
                                    <h4 class="modal-title"><b id="tieude">Đăng nhập</b></h4>
                                </a></li>
                            <li><a data-toggle="tab" href="#menu1">
                                    <h4 class="modal-title"><b id="tieude">Đăng ký</b></h4>
                                </a></li>

                        </ul>

                    </div>
                    <div class="modal-body">
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <form id="formModelLogin" onsubmit="return login(this.value)" method="post">

                                    <div class="input-group">
                                        <span id="status"></span>
                                    </div>
                                    <br>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                        <input id="email" type="text" class="form-control" name="email" placeholder="Tên đăng nhập">
                                    </div>
                                    <br>
                                    <div class="input-group">
                                        <span class="input-group-addon"> <i class="fa fa-key"></i></span>
                                        <input id="pass" type="password" class="form-control" name="pass" placeholder="Nhập mật khẩu">
                                    </div>
                                    <br>
                                    <input class="button form_submit" value="Đăng nhập" type="submit">
                                </form>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <form id="formModelRegister" onsubmit="return register()" method="post">

                                    <div class="input-group">
                                        <span id="statusRegister"></span>
                                    </div>
									<br>
									<h6>Thông tin cá nhân</h6>
									<hr>
                                    <div class="input-group">
									<span class="input-group-addon">Họ tên</span>
                                        <input id="hoTenr" type="text"  class="form-control" name="hoTen" 
                                            placeholder="Nhập họ tên" required>
                                    </div>
									<br>
								
									Giới tính <input  value="0"  checked="checked" name="gioiTinhr" type="radio"> Nam <input " value="1" type="radio" name="gioiTinhr"> Nữ
									<br>
									<br>
									<div class="input-group">
                                        <span class="input-group-addon">Công ty</span>
                                        <input id="congTyr" type="text" class="form-control" name="congTy"
                                            placeholder="Tên công ty">
									</div>
									
                                    <br>
									<br>
									<h6>Địa chỉ</h6>
									<hr>
									<div class="input-group">
									<span class="input-group-addon">Địa chỉ</span>
                                        <input id="diaChir" required type="text" class="form-control" name="diaChi" 
                                            placeholder="Nhập địa chỉ hiện tại">
									</div>
									<br>
									<div class="input-group">
									<span class="input-group-addon">Tỉnh/Thành phố</span>
									<select name="thanhPhor" id="" class="form-control">
                                    <?php while ($row = $tinh->unbuffered_row()) { ?>
									    <option <?php echo $row->matinh ?> >  <?php echo $row->ten ?> </option>
                                    <?php } ?>
									   </select>
									</div>
									
								

									<br>
									<br>
									<br>
									<h6>Thông tin liên hệ</h6>
									<hr>
									<div class="input-group">
									<span class="input-group-addon">Điện thoại</span>
                                        <input id="dienThoair" required type="text" class="form-control" name="dienThoai" 
                                            placeholder="Nhập họ tên">
									</div>
									
									<br>
									<div class="input-group">
									<span class="input-group-addon">Email</span>
                                        <input id="emailr" required type="email" class="form-control" name="emailr" 
                                            placeholder="Email">
									</div>
									<br>
									<div class="input-group">
									<span class="input-group-addon">Trang web</span>
                                        <input id="trangWebr"  type="text" class="form-control" name="trangWeb" 
                                            placeholder="Link website">
									</div>
									
									<br>
									<br>
									<br>
									<h6>Tên truy cập & mật khẩu</h6>
									<hr>
									<div class="input-group">
									<span class="input-group-addon">Tên đăng nhập</span>
                                        <input id="tenDangNhapr" required type="text" class="form-control" name="tenDangNhap" 
                                            placeholder="Tên đăng nhập">
									</div>
									<br>
									<div class="input-group">
									<span class="input-group-addon">Mật khẩu</span>
                                        <input id="matKhaur" required type="text" class="form-control" name="matKhau" 
                                            placeholder="Nhập mật khẩu">
									</div>
									<br>
									<div class="input-group">
									<span class="input-group-addon">Nhập lại mật khẩu</span>
                                        <input id="matKhau2r" required type="text" class="form-control" name="matKhau2" 
                                            placeholder="Nhập lại mật khẩu">
                                    </div>
                                    <br>
                                    <input class="button form_submit" value="Đăng ký" type="submit"> 
                                </form>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>







        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="teamplate/USER/frontend/js/home.js"></script>
       