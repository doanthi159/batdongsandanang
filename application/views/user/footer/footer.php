<footer id="footer">
			<div class="container">
				<div class="container_inner">
					<div class="company" data-aos="fade-right">
						<div class="footer_logo">
							<label>Công ty Cổ phần Bất Động Sản Đà Nẵng</label>
						</div>
						<div class="info">
							<p><i class="fa fa-map-marker" aria-hidden="true"></i>97/06 Nguyễn Lương Bằng, Hòa Khánh, Liên Chiểu, Đà Nẵng</p>

							<p><i class="fa fa-map-marker" aria-hidden="true"></i>VPĐD: 97/06 Nguyễn Lương Bằng, Hòa Khánh, Liên Chiểu, Đà Nẵng</p>
							<p><i class="fa fa-phone" aria-hidden="true"></i>0975 3579 60</p>
							<p><i class="fa fa-mobile" aria-hidden="true"></i>0975 3579 60 - 0916 461 613</a></p>
							<p><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:lienhe.batdongsandanang.com">lienhe.dxbmt@batdongsandanang.com</a></p>
						</div>
						<div class="services">
							<h3>Kết nối với chúng tôi</h3>
							<ul class="ul social">
								<li><a href="https://www.facebook.com/batdongsandanang" title="facebook" rel="nofollow" target="_blank"><i
										 class="fa fa-facebook delay" aria-hidden="true"></i></a></li>
								<li><a href="https://www.youtube.com/c/batdongsandanang" title="youtube" rel="nofollow" target="_blank"><i
										 class="fa fa-youtube-play delay" aria-hidden="true"></i></a></li>
								<!-- <li><a href="" title="Zalo" rel="nofollow"
									 target="_blank"><img src="frontend/images/zalo.png"></a></li> -->
							</ul>
						</div>
					</div>
					<div class="fan_page">
						<div class="fb-page" data-href="https://www.facebook.com/batdongsandanang/" data-tabs="timeline" data-height="250px"
						 data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
							<blockquote cite="https://www.facebook.com/batdongsandanang/" class="fb-xfbml-parse-ignore">
								<a href="https://www.facebook.com/batdongsandanang/"></a></blockquote>
						</div>
					</div>
					<div class="member">Member Of Bat Dong San Da Nang</div>
				</div>
				<div class="copyright">
					<p>Copyright @ 2017 - 2018</p>
					<p>Bat Dong San Da Nang JSC.</p>
				</div>
			</div>
		</footer>
		<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
