<section class="box_search">
				<div class="container">

					<div class="search">
						<div class="head_title">
							<div class="title"><span></span>Bất động sản</div>
						</div>
						<div class="searchWrapper">
							<form  method="get" action="">
									<input hidden="hidden" name='page' value='find'/>
								<div class="search-field input-field">
									<label>Loại dự án</label>
									<select name="loaiduan" >
										<option value="">Chọn loại dự án</option>
										<option value="4">Đất và Móng</option>
										<option value="3">Đất nền</option>
										<option value="2">Biệt thự</option>
										<option value="1">Nhà phố</option>
									</select>
								</div>
								
								
								<div class="search-field input-field">
									<label>Khoảng giá</label>
									<select name="gia">
										<option value=">0">Chọn khoảng giá</option>
										<option value="<1500000000">Dưới 1,5 tỷ</option>
										<option value="BETWEEN 1500000000 and 2500000000">Từ 1,5 - 2,5 tỷ</option>
										<option value="BETWEEN 1500000000 and 2500000000">Từ 2,5 - 3,5 tỷ</option>
										<option value="BETWEEN 3500000000 and 5000000000">Từ 3,5 - 5 tỷ</option>
										<option value="BETWEEN 5000000000 and 7000000000">Từ 5 - 7 tỷ</option>
										<option value="BETWEEN 7000000000 and 10000000000">Từ 7 - 10 tỷ</option>
										<option value=">10000000000">Trên 10 tỷ</option>
									</select>
								</div>
								<div class="search-field input-field">
									<label>Diện tích</label>
									<select name="dientich">
										<option value=">0">Chọn diện tích</option>
										<option value="<100">Dưới 100 m2</option>
										<option value="BETWEEN 100 and 200">Từ 100 - 200 m2</option>
										<option value="BETWEEN 200 and 300">Từ 200 - 300 m2</option>
										<option value="BETWEEN 300 and 500">Từ 300 - 500 m2</option>
										<option value=">500">Trên 500 m2</option>
									</select>
								</div>
								<div class="search-field input-field">
                                    <label>Tỉnh</label>
                                    <select name="tinh" onchange="gethuyen(this.options[this.selectedIndex].value)">
                                        <option value="">-------Chọn tỉnh thành -------------</option>
                                        <?php while ($row = $tinhthanh->unbuffered_row()) { ?>
										<option value="<?php echo $row->matinh ?>"><?php echo $row->ten ?></option>
										<?php } ?>
									</select>
                                </div>
                                <div class="search-field input-field">
                                    <label>Huyện</label>
                                    <div   id="divhuyen">
								<select class="form-control" disabled="disabled"  name="huyen" id="huyenoke" >
								<option value="">-------Chọn quận/huyện -------------</option>
								</select>
                                </div>
								<div class="search-field button-field">
									<!-- <input type="submit" class="button" value="Tìm kiếm"> -->
									<button class="button">Tìm kiếm</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>
