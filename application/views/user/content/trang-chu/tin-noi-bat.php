<section class="box_top">
				<div class="container">
					<div class="head_title">
						<div class="title"><span>Tin</span> Nổi bật</div>
					</div>
					<div class="box_news">
					<?php while ($row = $listtintuc1->unbuffered_row()) { ?>
						<div class="news news_list" data-aos="fade-up">
							<div class="item_first">
								<a class="image" href="?page=chitiettin&id=<?php echo $row->idtin ?>&idsame=<?php echo $row->idtintuc ?>"><img
									 src="teamplate/USER/img/<?php echo $row->anhminhhoa ?>" 
									 title="<?php echo $row->tenbaiviet ?>"></a>
								<div class="blog-dsc">
									<h4 class="title"><a href="?page=chitiettin&id=<?php echo $row->idtin ?>&idsame=<?php echo $row->idtintuc ?>"
										 title="<?php echo $row->tenbaiviet ?>"> <?php 
                                                    if(strlen(trim($row->tenbaiviet))>40){
                                                    echo substr(trim($row->tenbaiviet),  0, 40)."...";}else{echo $row->tenbaiviet;} ?></a></h4>
									<div class="description">
										<p><strong> <?php 
                                                    if(strlen(trim($row->tomtat))>52){
                                                    echo substr(trim($row->tomtat),  0, 52)."...";}else{echo $row->tomtat;} ?></strong></p>
									</div>
								</div>
							</div>
							<?php } ?>

							<div class="row slide_news">
							<?php while ($row = $listtintuc2->unbuffered_row()) { ?>
								<div class="col-xs-12 col-sm-6 col-md-4 mb-40">
									<div class="item">
										<a class="image" href="?page=chitiettin&id=<?php echo $row->idtin ?>&idsame=<?php echo $row->idtintuc ?>"><img
											 src="teamplate/USER/img/<?php echo $row->anhminhhoa ?>" title="<?php echo $row->tenbaiviet ?>"></a>
										<div class="blog-dsc">
											<h4 class="title"><a href="?page=chitiettin&id=<?php echo $row->idtin ?>&idsame=<?php echo $row->idtintuc ?>"
												 title=<?php echo $row->tenbaiviet ?>"><?php 
                                                    if(strlen(trim($row->tenbaiviet))>40){
                                                    echo substr(trim($row->tenbaiviet),  0, 40)."...";}else{echo $row->tenbaiviet;} ?></a></h4>
											<div class="description"> <?php 
                                                    if(strlen(trim($row->tomtat))>52){
                                                    echo substr(trim($row->tomtat),  0, 52)."...";}else{echo $row->tomtat;} ?></div>
										</div>
									</div>
								</div>
							<?php } ?>
								
									


							</div>
						</div>

						<script>
							$(document).ready(function () {
								$('.slide_news').slick({
									dots: true,
									infinite: true,
									arrows: false,
									speed: 300,
									autoplay: true,
									autoplaySpeed: 2000,
									slidesToShow: 3,
									slidesToScroll: 2,
									responsive: [
										{
											breakpoint: 900,
											settings: {
												slidesToShow: 2
											}
										},
										{
											breakpoint: 600,
											settings: {
												slidesToShow: 1,
												slidesToScroll: 1
											}
										}
										// You can unslick at a given breakpoint now by adding:
										// settings: "unslick"
										// instead of a settings object
									]
								});
							});
						</script>
					</div>
					
				
				</div>
			</section>