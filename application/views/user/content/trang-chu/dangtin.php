<section id="contact">
	<div class="container">
    <div class="head_title">
		<div class="title"><span></span>Đăng Tin</div>
	</div>
		<div class="contact">
			<!-- <h2 class="title">Liên hệ với chúng tôi</h2> -->
			<div id="contact_inner">
        <div id="sidebar">
          <div class="sidebar__inner">
            <div class="sidebar sb_contact">
	<h5>Liên hệ chúng tôi</h5>
	<div class="contactWrapper">
		<div class="item">
			<div class="title">Văn phòng</div>
			<div class="desc"><i class="fa fa-map-marker"></i><a class="address" href="#">Lô A3 Hoàng Quốc Việt, phường An Đông, Tp. Huế</a></div>
		</div>
		
		<div class="item">
			<div class="title">Điện thoại</div>
			<div class="desc"><i class="fa fa-phone"></i><a class="phone" href="tel:02343998999">02343 998 999</a></div>
		</div>
		<div class="item">
			<div class="title">Di động</div>
			<div class="desc"><i class="fa fa-mobile"></i><a class="phone" href="tel:0964551888">0964 551 888 - 0931 992 123</a></div>
		</div>
		<div class="item">
			<div class="title">E-mail</div>
			<div class="desc"><i class="fa fa-envelope"></i><a class="mail" href="mailto:utduong77@gmail.com">lienhe.dxbmt@datxanhmientrung.com</a></div>
		</div>
	</div>
</div>          </div>
        </div>
       
				<div class="contact-form">
					<h2 class="title">Thông tin cần đăng</h2>
					<div id="loading"><img src="" /></div>
					<div class="notice success">Bạn đã gửi thông tin thành công!</div>
					<div class="notice error">Thông tin gửi không hoàn thành! Vui lòng thử lại!</div>
                    <div class="notice success">Bạn đã gửi thông tin thành công!</div>
					<form  method="post" action="index.php/dangtin"  onsubmit="return checkpost()" enctype="multipart/form-data">
						
					<div class="field half">
						<b>Tiêu đề: <b><br>
						<input class="form-control" type="text" rows="5" name="tieude" required>
						</div>
						<div class="field half">
							<b>Hình thức: <b><br>
							<select  name="hinhThuc" style="width:40%;" class="form-control">
							<?php while ($row = $danhmuc->unbuffered_row()) { ?>
										<option value="<?php echo $row->id_dm ?>"><?php echo $row->tieude ?></option>
							<?php } ?>
									</select> 
						</div>
						<br>
						<div class="field half">
						<b>Loại BDS: <b><br>
						<select  name="loaiBDS" style="width:40%;" class="form-control">
										<option value="1">Nhà Phố</option>
										<option value="2">Biệt Thự</option>
										<option value="3">Đất Nền</option>
										<option value="4">Đất Và Móng</option>
									</select> 
						</div>
						<br>
						
						<div class="field half">
						<b>Tỉnh thành: <b><br>
						<select  name="tinh" style="width:40%;" id="tinhoke" onchange="gethuyen(this.options[this.selectedIndex].value)" class="form-control">
						<option value="">-------Chọn tỉnh thành -------------</option>
						<?php while ($row = $tinhthanh->unbuffered_row()) { ?>
										<option value="<?php echo $row->matinh ?>"><?php echo $row->ten ?></option>
										<?php } ?>
									</select> 
						</div>
						<br>
						<div class="field half">
						<b>Quận/Huyện: <b><br>
						<div   id="divhuyen">
								<select class="form-control" disabled="disabled" style="width:40%;"  name="huyen" id="huyenoke" >
								<option value="">-------Chọn quận/huyện -------------</option>
								
								
							</select>
						</div>
						</div>
						<br>
						<div class="field half">
						<b>Địa chỉ: <b><br>
						    <input class="text-input small-input" type="text" name="diaChi" required/> 
						</div>
						<br>
						<div class="field half">
						<b>Diện tích khu vực (km2): <b><br>
						    <input class="text-input small-input" type="text" name="dienTichKV" required/> 
						</div>
						<br>
						<div class="field half">
						<b>Diện tích sàn XD (km2): <b><br>
						    <input class="text-input small-input" type="text" name="dienTichSan" /> 
						</div>
						<br>
						<div class="field half">
						<b>Tổng Diện tích XD (km2): <b><br>
						    <input class="text-input small-input" type="text" name="dienTichXD" /> 
						</div>
						<br>

						<div class="field half">
						<b>Hiện trạng (km2): <b><br>
						    <input class="text-input small-input" type="text" name="hienTrang" /> 
						</div>
						<br>

						<div class="field half">
						<b>Quy mô (km2): <b><br>
						    <input class="text-input small-input" type="text" name="quyMo" /> 
						</div>
						<br>
						<div class="field half">
						<b>Hướng nhà: <b><br>
						<select name="huongNha" style="width:40%;" class="form-control">

										<option value="Đông">Đông</option>
										<option value="Đông">Tây</option>
										<option value="Đông">Nam</option>
										<option value="Đông">Bắc</option>
									
									</select> 
						</div>
						<br>

						<div class="field half">
						<b>Hướng ban công: <b><br>
						<select  name="huongBanCong" style="width:40%;" class="form-control">

										<option value="Đông">Đông</option>
										<option value="Đông">Tây</option>
										<option value="Đông">Nam</option>
										<option value="Đông">Bắc</option>
									
						</select> 
						</div>
						<br>
						<div class="field half">
						<b>Mặt tiền rộng: <b><br>
						<input class="text-input small-input" type="text" name="matTien" /> 
						</div>
						<br>
						<div class="field half">
						<b>Đường mặt tiền: <b><br>
						<input class="text-input small-input" type="text" name="duongMatTien" /> 
						</div>
						<br>
						<div class="field half">
						<b>Giá: <b><br>
						<input class="text-input small-input" type="text" name="gia" required/>  VND
						</div>
						<br>

						<div class="field half">
						<b>Ảnh minh họa: <b><br>
						    <input class="text-input small-input" type="file" id="small-input" name="anhMinhHoa" required/> 
						</div>
						<br>
						
						<div class="field half">
						<b>Tóm tắt ngắn: <b><br>
						<textarea class="form-control" rows="5" name="tomTat" required></textarea>
						</div>
						<br>

						<div class="field">
						<b>Mô tả chi tiết: <b><br>
                        <textarea class="form-control" rows="5" name="noidung" id="input"  required></textarea>
										<script type="text/javascript">
											CKEDITOR.replace( 'noidung', {
												filebrowserBrowseUrl: 'teamplate/ADMIN/ckfinder/ckfinder.html',
												filebrowserImageBrowseUrl: 'teamplate/ADMIN/ckfinder/ckfinder.html?type=Images',
												filebrowserFlashBrowseUrl: 'teamplate/ADMIN/ckfinder/ckfinder.html?type=Flash',
												filebrowserUploadUrl: 'teamplate/ADMIN/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
												filebrowserImageUploadUrl: 'teamplate/ADMIN/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
												filebrowserFlashUploadUrl: 'teamplate/ADMIN/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
												
											} );
										</script>
                        </div>
                        <br>
						<input type="submit" value="Gửi  " name="subscribe" class="button submit" id="subscribe">
					</form>
				</div>
				
				<div class="clear"></div>
			</div>
		</div>
	</div>
</section>  