<section class="box_why">
				<div class="container">
					<div class="head_title">
						<div class="title"><span>Chúng tôi</span> Cam kết</div>
					</div>
					<div class="contentWrapper">
						<div class="content">
							<p>Công ty CP Đất Xanh Bắc Miền Trung là thành viên của Tập đoàn Đất Xanh. Thương hiệu uy tín trong lĩnh vực đầu
								tư và phân phối bất động sản lớn nhất Việt Nam. Đất Xanh Bắc Miền Trung là một đơn vị chủ lực kinh doanh với
								hàng chục dự án về đất nền, biệt thự nghỉ dưỡng cao cấp, các khu đô thị ở khu vực Huế và Miền Trung. Không
								ngừng tìm kiếm và tạo ra hiệu quả về kinh doanh, tài chính lành mạnh cho nhà đầu tư, đối tác kinh doanh, tạo cơ
								hội phát triển và đem lại phúc lợi cho CBNV cũng như an sinh xã hội.</p>
							<p>Với sứ mệnh “đồng hành cùng sự phát triển của Khách hàng”chúng tôi đã không ngừng tư vấn, cung cấp dịch vụ
								bất động sản chuyên nghiệp, uy tín hàng đầu ở khu vực Bắc Miền Trung cũng như cả nước.</p>
						</div>
						<div class="image-list">
							<div class="large-image">
								<img src="teamplate/USER/frontend/images/allteam.jpg">
							</div>
							<div class="list_item">
								<div class="item">
									<div class="image"><i class="material-icons">assignment_turned_in</i><!-- <img src="http://ld-wp.template-help.com/wordpress_bemedical/v4/healtix/wp-content/uploads/2017/10/icon-img-1.png" > -->
									</div>
									<div class="blog">
										<h5>Thông tin nhanh chóng</h5>
										<p>Cập nhật thông tin, tiến độ dự án chính xác nhất, mới nhất.</p>
									</div>
								</div>
								<div class="item">
									<div class="image"><i class="material-icons">thumb_up</i><!-- <img src="http://ld-wp.template-help.com/wordpress_bemedical/v4/healtix/wp-content/uploads/2017/10/icon-img-2.png" > -->
									</div>
									<div class="blog">
										<h5>Đối tác uy tín</h5>
										<p>Pháp lý rõ ràng. Chính sách bán hàng tốt nhất. Đảm bảo quyền lợi khách hàng</p>
									</div>
								</div>
								<div class="item">
									<div class="image"><i class="material-icons">verified_user</i><!-- <img src="http://ld-wp.template-help.com/wordpress_bemedical/v4/healtix/wp-content/uploads/2017/10/icon-img-3.png" > -->
									</div>
									<div class="blog">
										<h5>Chất lượng và an toàn</h5>
										<p>Cam kết chất lượng BĐS, đảm bảo tối đa quyền lợi và tiêu dùng cho khách hàng đầu tư và sử dụng.</p>
									</div>
								</div>
								<div class="item">
									<div class="image"><i class="material-icons">perm_phone_msg</i><!-- <img src="http://ld-wp.template-help.com/wordpress_bemedical/v4/healtix/wp-content/uploads/2017/10/icon-img-4.png" > -->
									</div>
									<div class="blog">
										<h5>Hỗ trợ tức thời</h5>
										<p>Không chỉ có được sự tư vấn, hỗ trợ tối đa và nhanh nhất. Việc liên hệ của khách hàng sẽ được chúng tôi
											tiếp nhận Mọi lúc mọi nơi.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>


<section class="box_quote">
				<div class="container">
					<div class="head_title">
						<div class="title">Cam kết - Nhận định</div>
					</div>
					<div class="quote-slide">
						<div class="item">
							<figure>
								<div class="avatar" style="background-image:url('teamplate/USER/frontend/images/congthanhI.jpg')"><img
									 src="teamplate/USER/frontend/images/congthanhI.jpg"></div>
								<blockquote>
									<span class="name">Tống Phước Hoàng Hưng<span class="pos">Tổng Giám Đốc</span></span>
									<p><i class="fa fa-quote-left"></i>Chúng tôi cam kết cùng với lãnh đạo tỉnh Thừa Thiên Huế sẽ giới thiệu cho
										khách hàng nhiều dự án, khu đô thị kiểu mẫu và đồng hành, phát triển bất động sản Huế lên một tầm cao mới.<i
										 class="fa fa-quote-right"></i></p>
								</blockquote>
							</figure>
						</div>
						<div class="item">
							<figure>
								<div class="avatar" style="background-image:url('teamplate/USER/frontend/images/congthanhI.jpg')"><img
									 src="teamplate/USER/frontend/images/congthanhI.jpg"></div>
								<blockquote>
									<span class="name">Dương Công Thành<span class="pos">Kế Toán Trưởng</span></span>
									<p><i class="fa fa-quote-left"></i>Chúng tôi cam kết rằng, khách hàng khi đầu tư vào Đất Xanh, chúng tôi sẽ
										tạo ra một môi trường đầu tư ổn định và yên tâm nhất.<i class="fa fa-quote-right"></i></p>
								</blockquote>
							</figure>
						</div>
						<div class="item">
							<figure>
								<div class="avatar" style="background-image:url('teamplate/USER/frontend/images/gdkd1.jpg')"><img
									 src="teamplate/USER/frontend/images/gdkd1.jpg"></div>
								<blockquote>
									<span class="name">Huỳnh Ngọc Hưng<span class="pos">Giám Đốc Kinh Doanh</span></span>
									<p><i class="fa fa-quote-left"></i>Chúng tôi luôn có những định hướng phát triển mới, tiên phong đối với thị
										trường bất động sản Huế.<i class="fa fa-quote-right"></i></p>
								</blockquote>
							</figure>
						</div>
					</div>
				</div>
			</section>

			<script type="text/javascript">
				$('.quote-slide').slick({
					dots: true,
					infinite: true,
					arrows: false,
					autoplay: true
				});
			</script>