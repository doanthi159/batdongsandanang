<section class="box_project">
				<div class="container">
					<div class="head_title">
						<div class="title"><span>Dự án</span> Nổi bật</div>
					</div>
					<div class="row">
					<?php while ($row = $listduan1->unbuffered_row()) { ?>
						<div class="col-xs-12 col-sm-6" data-aos="fade-right">
							<div class="item first">
								<a href="?page=bantin&id=<?php echo $row->idbaiviet ?>&idsame=<?php echo $row->id_dm ?>" style="background-image: url('teamplate/USER/img/<?php echo $row->anhminhhoa ?>')">
								<img src="teamplate/USER/img/<?php echo $row->anhminhhoa ?>">
									<div class="blog">
									<?php 
                                                    if(strlen(trim($row->tenbaiviet))>30){
                                                    echo substr(trim($row->tenbaiviet),  0, 30)."...";}else{echo $row->tenbaiviet;} ?>
										<span class="desc"> <?php 
                                                    if(strlen(trim($row->tomtat))>50){
                                                    echo substr(trim($row->tomtat),  0, 50)."...";}else{echo $row->tomtat;} ?></span>
									</div>
								</a>
							</div>
						</div>
					<?php } ?>

					<?php while ($row = $listduan2->unbuffered_row()) { ?>

						<div class="col-xs-12 col-sm-6" data-aos="fade-left">
							<div class="col-xs-12">
								<div class="item second">
										<a href="?page=bantin&id=<?php echo $row->idbaiviet ?>&idsame=<?php echo $row->id_dm ?>" style="background-image: url('teamplate/USER/img/<?php echo $row->anhminhhoa ?>')">
										<img src="teamplate/USER/img/<?php echo $row->anhminhhoa ?>">
											<div class="blog">
											<?php 
                                                    if(strlen(trim($row->tenbaiviet))>30){
                                                    echo substr(trim($row->tenbaiviet),  0, 30)."...";}else{echo $row->tenbaiviet;} ?>
										<span class="desc"> <?php 
                                                    if(strlen(trim($row->tomtat))>50){
                                                    echo substr(trim($row->tomtat),  0, 50)."...";}else{echo $row->tomtat;} ?></span>
											</div>
										</a>
								</div>
							</div>
							<?php } ?>

							<?php while ($row = $listduan3->unbuffered_row()) { ?>
							<div class="col-xs-12 col-sm-6">
								<div class="item third">
										<a href="?page=bantin&id=<?php echo $row->idbaiviet ?>&idsame=<?php echo $row->id_dm ?>" style="background-image: url('teamplate/USER/img/<?php echo $row->anhminhhoa ?>')">
										<img src="teamplate/USER/img/<?php echo $row->anhminhhoa ?>">
											<div class="blog">
											<?php 
                                                    if(strlen(trim($row->tenbaiviet))>30){
                                                    echo substr(trim($row->tenbaiviet),  0, 30)."...";}else{echo $row->tenbaiviet;} ?>
										<span class="desc"> <?php 
                                                    if(strlen(trim($row->tomtat))>50){
                                                    echo substr(trim($row->tomtat),  0, 50)."...";}else{echo $row->tomtat;} ?></span>
											</div>
										</a>
								</div>
							</div>
							<?php } ?>

							<?php while ($row = $listduan4->unbuffered_row()) { ?>
							<div class="col-xs-12 col-sm-6">
								<div class="item fourth">
										<a href="?page=bantin&id=<?php echo $row->idbaiviet ?>&idsame=<?php echo $row->id_dm ?>" style="background-image: url('teamplate/USER/img/<?php echo $row->anhminhhoa ?>')">
										  <img src="teamplate/USER/img/<?php echo $row->anhminhhoa ?>">
											<div class="blog">
											<?php 
                                                    if(strlen(trim($row->tenbaiviet))>30){
                                                    echo substr(trim($row->tenbaiviet),  0, 30)."...";}else{echo $row->tenbaiviet;} ?>
										<span class="desc"> <?php 
                                                    if(strlen(trim($row->tomtat))>50){
                                                    echo substr(trim($row->tomtat),  0, 50)."...";}else{echo $row->tomtat;} ?></span>
											</div>
										</a>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</section>