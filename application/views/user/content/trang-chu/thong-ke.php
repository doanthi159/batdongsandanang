<section class="box_achievements">
				<div class="container">
					<div class="contentWrapper">
						<div class="item">
							<span class="counterup" data-count="10">0</span>
							<h4>Dự án đầu tư thứ cấp</h4>
						</div>
						<div class="item">
							<span class="counterup" data-count="20">0</span>
							<h4>Dự án mở bán</h4>
						</div>
						<div class="item">
							+ <span class="counterup" data-count="1004">0</span>
							<h4>Giao dịch BĐS</h4>
						</div>
						<div class="item">
							+ <span class="counterup" data-count="3650">0</span>
							<h4>Khách hàng phát sinh</h4>
						</div>
					</div>
				</div>
            </section>
            <script type="text/javascript">
				var a = 0;
				$(window).scroll(function () {
					var oTop = $('.box_achievements').offset().top - window.innerHeight;
					if (a == 0 && $(window).scrollTop() > oTop) {
						$('.counterup').each(function () {
							var $this = $(this),
								countTo = $this.attr('data-count');
							$({
								countNum: $this.text()
							}).animate({ countNum: countTo }, {
								duration: 2000,
								easing: 'swing',
								step: function () {
									$this.text(Math.floor(this.countNum));
								},
								complete: function () {
									$this.text(this.countNum);
								}
							});
						});
						a = 1;
					}
				});
			</script>