<!-- MAIN-CONTNENT -->
<div id="main-body">

	<section class="breadcrumbs">
		<div class="container">
			<div class="content">
				<span class="item"><a href="?page=trangchu" title="Trang chủ">Trang chủ</a></span>
				<span class="item_sep"> / </span>
				<span class="item"><a href="?page=gioithieu" title="Giới thiệu">Giới thiệu</a></span>
			</div>
		</div>
	</section>

	<section id="news">
		<div class="container">
			<div class="news news_detail introduction">
				<div class="module">
					<div class="moduletitle">
						<h1>CÔNG TY CỔ PHẦN ĐẤT XANH BẮC MIỀN TRUNG</h1>
						<h2>Về chúng tôi</h2>
					</div>

					<div class="modulecontent">
						<div class="item">
							<div class="content-detail">
								<div class="description">
									<p><span style="font-size:16px">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; C&ocirc;ng ty CP Đất Xanh Bắc Miền Trung
											l&agrave; th&agrave;nh vi&ecirc;n của Tập đo&agrave;n Đất Xanh. Thương hiệu uy t&iacute;n trong lĩnh vực đầu
											tư v&agrave; ph&acirc;n phối bất động sản lớn nhất Việt Nam. Đất Xanh Bắc Miền Trung l&agrave; một đơn vị
											chủ lực kinh doanh với h&agrave;ng chục dự &aacute;n về đất nền, biệt thự nghỉ dưỡng cao cấp, c&aacute;c khu
											đ&ocirc; thị ở khu vực Huế v&agrave; Miền Trung. Kh&ocirc;ng ngừng t&igrave;m kiếm v&agrave; tạo ra hiệu quả
											về kinh doanh, t&agrave;i ch&iacute;nh l&agrave;nh mạnh cho nh&agrave; đầu tư, đối t&aacute;c kinh doanh,
											tạo cơ hội ph&aacute;t triển v&agrave; đem lại ph&uacute;c lợi cho CBNV cũng như an sinh x&atilde; hội.</span></p>

									<p><span style="font-size:16px">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Với sứ mệnh &ldquo;đồng h&agrave;nh
											c&ugrave;ng sự ph&aacute;t triển của Kh&aacute;ch h&agrave;ng&rdquo;ch&uacute;ng t&ocirc;i đ&atilde;
											kh&ocirc;ng ngừng tư vấn, cung cấp dịch vụ bất động sản chuy&ecirc;n nghiệp, uy t&iacute;n h&agrave;ng đầu ở
											khu vực Bắc Miền Trung cũng như cả nước.</span></p>
								</div>
								<div class="block_history">
									<ul class="nav nav-tabs responsive history" role="tablist">
										<li class="nav-item">
										</li>
										<li class="nav-item">
											<a class="nav-link tab-1" href="javascript:showItem('tab-1');">2003</a>
										</li>
										<li class="nav-item">
										</li>
										<li class="nav-item">
										</li>
										<li class="nav-item">
										</li>
										<li class="nav-item">
										</li>
										<li class="nav-item">
										</li>
										<li class="nav-item">
										</li>
										<li class="nav-item">
										</li>
										<li class="nav-item">
											<a class="nav-link tab-9" href="javascript:showItem('tab-9');">2011</a>
										</li>
										<li class="nav-item">
										</li>
										<li class="nav-item">
										</li>
										<li class="nav-item">
										</li>
										<li class="nav-item">
										</li>
										<li class="nav-item">
											<a class="nav-link tab-14" href="javascript:showItem('tab-14');">2016</a>
										</li>
										<li class="nav-item">
											<a class="nav-link tab-15 active" href="javascript:showItem('tab-15');">2017</a>
										</li>
									</ul>
									<div class="tab-content responsive">
										<div class="tab-pane" id="tab-1">
											<div class="contentFull">
												<p><strong>2003</strong> – Thành lập Công ty TNHH Dịch vụ và Xây dựng Địa ốc Đất Xanh</p>
											</div>
										</div>
										<div class="tab-pane" id="tab-9">
											<div class="contentFull">
												<p><strong>Tháng 3/2011</strong> - Tái cơ cấu Đất Xanh lên mô hình Tập đoàn Đất Xanh (Dat Xanh Group -
													DXG).</p>
												<p><strong>Tháng 4/2011</strong> - Thành lập Công ty Cổ phần Đất Xanh Miền Trung.</p>
											</div>
										</div>
										<div class="tab-pane" id="tab-14">
											<div class="contentFull">
												<p><strong>Tháng 01/2016</strong> - Thành lập chi nhánh Bắc Miền Trung thuộc CTCP Đất Xanh Miền Trung tại
													Thừa Thiên Huế.</p>
											</div>
										</div>
										<div class="tab-pane active" id="tab-15">
											<div class="contentFull">
												<p><strong>Tháng 06/2017</strong> - Thành lập Công ty Cổ phần Đất Xanh Bắc Miền Trung.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="content"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="box_team">
		<div class="container">
			<div class="head_title">
				<div class="title"><span>Ban Giám đốc</span></div>
			</div>
			<div class="contentWrapper">
				<div class="content">
					<div class="main">
						<div class="item">
							<div class="wrap">
								<div class="body">
									<div class="image">
										<div class="photo">
											<a href=""><img src="teamplate/USER/frontend/images/tgd.jpg" class="avatar"></a>
										</div>
									</div>
									<div class="person_info">
										<p class="position"><span>Tổng Giám đốc</span></p>
										<h5 class="name"><a href="">Ông Tống Phước Hoàng Hưng</a></h5>
										<!-- <div class="macros"><span class="phone">+ 800 123 45 67</span></div>
												<div class="macros"><a href="mailto:info@beclinic.com" class="email">info@beclinic.com</a></div> -->
									</div>
								</div>
							</div>
							<div class="intro">
								<p>Thành lập công ty vào tháng 6/2017, qua hơn 1 năm hình thành và phát triển trên thị trường Huế, Ông Tống
									Phước Hoàng Hưng đã đưa Đất Xanh Bắc Miền Trung từ một chi nhánh trực thuộc Đất Xanh Miền Trung trở thành đơn
									vị phân phối bất động sản uy tín hàng đầu tại Thừa Thiên Huế nói riêng và khu vực Bắc Miền Trung nói chung,
									với tổng số vốn gấp 5 lần ban đầu. Từng học tập, công tác tại Châu Âu, ông luôn mong muốn mang tư duy mới của
									thị trường quốc tế vào Việt Nam, nỗ lực kiện toàn hệ thống để trở thành công ty dịch vụ bất động sản chuyên
									nghiệp trên thị trường, để khách hàng có thể sở hữu căn hộ chất lượng cao với mức giá tốt nhất và dịch vụ hoàn
									hảo.</p>
							</div>
						</div>
					</div>
					<div class="listing">
						<div class="item">
							<div class="wrap">
								<div class="body">
									<div class="image">
										<div class="photo">
											<a href=""><img src="teamplate/USER/frontend/images/gdkd1.jpg" class="avatar"></a>
										</div>
									</div>
									<div class="person_info">
										<p class="position"><span>Giám đốc Kinh doanh</span></p>
										<h5 class="name"><a href="">Ông Huỳnh Ngọc Hưng</a></h5>
									</div>
								</div>
							</div>
							<div class="intro">
								<p>Gia nhập Đất Xanh Bắc Miền Trung từ những ngày đầu thành lập, ông Huỳnh Ngọc Hưng là người đứng đầu khối
									kinh doanh, hoạch định chiến lược kinh doanh, quản lý đội ngũ sales vô cùng hiệu quả, chuyên nghiệp. Những
									đóng góp của ông đã thúc đẩy thành công cho hàng loạt giao dịch bất động sản của công ty trong thời gian qua.</p>
							</div>
						</div>
						<div class="item">
							<div class="wrap">
								<div class="body">
									<div class="image">
										<div class="photo">
											<a href=""><img src="teamplate/USER/frontend/images/ktt1.jpg" class="avatar"></a>
										</div>
									</div>
									<div class="person_info">
										<p class="position"><span>Giám đốc Kinh doanh</span></p>
										<h5 class="name"><a href="">Ông Bùi Huỳnh Tài</a></h5>
									</div>
								</div>
							</div>
							<div class="intro">
								<p>Ông Bùi Huỳnh Tài là người cố vấn, xây dựng và cung cấp giải pháp tài chính, đầu tư cho Tổng giám đốc. Thời
									gian làm việc tại Đất Xanh, ông đã giúp công ty quản lý cấu trúc vốn, xoay chuyển dòng tiền một cách hiệu quả,
									đảm bảo nguồn tài chính cho các hoạt động kinh doanh giúp tạo dựng niềm tin, uy tín của Đất Xanh Bắc Miền
									Trung với đối tác trên thị trường.</p>
							</div>
						</div>
						<div class="item">
							<div class="wrap">
								<div class="body">
									<div class="image">
										<div class="photo">
											<a href=""><img src="teamplate/USER/frontend/images/ktt1.jpg" class="avatar"></a>
										</div>
									</div>
									<div class="person_info">
										<p class="position"><span>TP. HC-NS</span></p>
										<h5 class="name"><a href="">Ông Nguyễn Đình Toản</a></h5>
									</div>
								</div>
							</div>
							<div class="intro">
								<p>Ông Nguyễn Đình Toản là người đặt những nền móng vững chắc cho hệ thống nhân sự, văn hóa của công ty. Sự
									chuyên nghiệp và gắn kết của các bộ phận tạo nên sức mạnh tập thể đáng tự hào.</p>
							</div>
						</div>
						<div class="item">
							<div class="wrap">
								<div class="body">
									<div class="image">
										<div class="photo">
											<a href=""><img src="teamplate/USER/frontend/images/ktt1.jpg" class="avatar"></a>
										</div>
									</div>
									<div class="person_info">
										<p class="position"><span>TP. Marketing</span></p>
										<h5 class="name"><a href="">Bà Thái Thị Thu</a></h5>
									</div>
								</div>
							</div>
							<div class="intro">
								<p>Bà Thái Thị Thu là người đi đầu trong việc xây dựng thương hiệu, đa dạng hóa kênh truyền thông marketing,
									chuyên nghiệp hóa tài liệu bán hàng, đồng bộ quy trình Marketing… giúp Đất Xanh Bắc Miền Trung ngày càng nhận
									được sự tin cậy của khách hàng và đối tác.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="multimedia">
		<div class="container">
			<div class="head_title">
				<div class="title"><span>Thư viện ảnh</span></div>
			</div>
			<div class="multimedia multimedia_list">
				<div class="module">
					<div class="modulecontent">
						<div class="list_item">
							<!-- anh-->
							<div class="item" data-aos="fade-up">
								<a class="read_more frm-product" data-fancybox="group-image-31" href="teamplate/USER/uploads/static/ban-tin/ban-tin1.JPG">
									<div class="image">
										<img src="teamplate/USER/uploads/static/ban-tin/ban-tin1.JPG" />
									</div>
									<div class="overlay">
										<div class="detail">
											<p class="title">Xem thêm</p>
										</div>
									</div>
									<div class="hidden">
									<?php while ($row = $listpig->unbuffered_row()) { ?>
										<p class="fancybox" data-fancybox="group-image-31" href="teamplate/USER/pig/<?php echo $row->link ?>"
										 title="<?php echo $row->tieu_de ?>"></p>
									<?php } ?>

										
									</div>
									<span class="title">Tổng kết hoạt động kinh doanh 2018 - BDS Đà Nẵng <i></i></span>
								</a>
							</div>
							<!-- anh-->
						


						</div>
					</div>

				</div>
			</div>
		</div>
	</section>




	<script type="text/javascript">
		$('.listing').slick({
			dots: true,
			infinite: false,
			speed: 300,
			slidesToShow: 3,
			slidesToScroll: 3,
			arrows: false,
			autoplay: true,
			responsive: [
				{
					breakpoint: 1000,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 650,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
	</script>

	<script>
		$(document).ready(function () {
			$('[data-fancybox="group-image"]').fancybox({
				loop: true,
				thumbs: {
					autoStart: true
				},
				buttons: ['zoom', 'close']
			});
		});

		// Button Viewmore featured
		$('.multimedia_list .more a').click(function () {

			$(this).parent().prev().fadeIn('fast');
			$('.more .readmore').fadeOut('fast');

			var uri = "";
			var num_items = $('#multimedia .multimedia_list .modulecontent .list_item .item').length;
			$.get(uri, { num_items: num_items }, function (data) {
				$('.loading').fadeOut('fast');
				$('#multimedia .multimedia_list .modulecontent .list_item').append(data);

				var num_items_new = $('#multimedia .multimedia_list .modulecontent .list_item .item').length;
				if ((num_items + 6) == num_items_new) {
					$('.more .readmore').fadeIn('fast');
				} else {
					$('#multimedia .multimedia_list .more').hide();
				}
			});
		});
	</script>

	<script type="text/javascript">
		function showItem(id) {
			$(".tab-pane").removeClass("active");
			$("#" + id).addClass("active");
			$(".nav-link").removeClass("active");
			$('a."' + id + '"]').addClass("active");
		};
	</script>
</div>

<!-- FOOTER -->