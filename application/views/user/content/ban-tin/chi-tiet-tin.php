<div id="main-body">

    <section class="breadcrumbs">
        <div class="container">
            <div class="content">
                <span class="item"><a href="?page=trangchu" title="Trang chủ">Trang chủ</a></span>
                <span class="item_sep"> / </span>
                <span class="item"><a href="?page=bantin" title="Tin Thị trường">Tin
                        Thị trường</a></span>
            </div>
        </div>
    </section>
    <?php while ($row = $bantin->unbuffered_row()) { ?>
    <section id="news">
        <div class="container">
            <div id="main-content" class="main">

                <div id="sidebar">
                    <div class="sidebar__inner">
                        <div class="news news_list relative right">
                            <div class="module">
                                <h2>Tin cùng chuyên mục</h2>
                                <div class="modulecontent">
                                <?php while ($row2 = $bantincung->unbuffered_row()) { ?>
                                    <div class="item">
                                        <a class="image" href="?page=chitiettin&id=<?php echo $row2->idtin ?>&idsame=<?php echo $row2->idtintuc ?>" title="Giá đất Huế liên tục tăng mạnh nhờ thúc đẩy hoàn thiện hạ tầng">
                                            <img src="teamplate/USER/uploads/static/ban-tin/ban-tin1.JPG" title="Giá đất Huế liên tục tăng mạnh nhờ thúc đẩy hoàn thiện hạ tầng"></a>
                                        <div class="blog-dsc">
                                            <span class="date"><?php echo $row2->createdate ?></span>
                                            <h4 class="title"><a href="?page=chitiettin&id=<?php echo $row2->idtin ?>&idsame=<?php echo $row2->idtintuc ?>" title="<?php echo $row2->tenbaiviet ?>">Giá
                                            <?php echo $row2->tenbaiviet ?></a></h4>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="center">
                    <div class="news news_detail">
                        <div class="module">
                            <div class="moduletitle">
                                <h1><?php echo $row->tenbaiviet ?></h1>
                            </div>

                            <div class="social-date">
                                <div class="fblike">
                                    <div class="fb-like" data-href="http://localhost/BatDongSanDaNang/?page=tintuc&id=<?php echo $id; ?>&idsame=<?php echo $idsame; ?>"
                                        data-layout="button_count" data-action="like" data-size="small" data-show-faces="true"
                                        data-share="true"></div>
                                    <div class="fb-send" data-href="http://localhost/BatDongSanDaNang/?page=tintuc&id=<?php echo $id; ?>&idsame=<?php echo $idsame; ?>"></div>
                                </div>
                                <span class="published"><?php echo $row->createdate ?></span>
                            </div>

                            <div class="modulecontent">
                                <div class="item">
                                    <div class="content-detail">
                                        <!-- noi dung-->
                                        <?php echo $row->noidung ?>
                                        <!-- end noi dung-->
                                    </div>
                                </div>
                                <div class="fbcomment">
                                    <div class="fb-comments" data-href="http://localhost/BatDongSanDaNang/?page=tintuc&id=<?php echo $id; ?>&idsame=<?php echo $idsame; ?>"
                                        data-width="100%" data-numposts="5"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <?php } ?>
    <script type="text/javascript">
        var stickySidebar = new StickySidebar('#sidebar', {
            topSpacing: 90,
            bottomSpacing: 20,
            containerSelector: '#main-content',
            innerWrapperSelector: '.sidebar__inner'
        });
    </script>
</div>