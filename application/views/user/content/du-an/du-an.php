<div id="main-body">

       
        <section class="breadcrumbs">
                <div class="container">
                    <div class="content">
                        <span class="item"><a href="?page=trangchu" title="Trang chủ">Trang chủ</a></span>
                        <span class="item_sep"> / </span>
                        <span class="item"><a href="?page=duan" title="Dự án">Dự án</a></span>
                    </div>
                </div>
            </section>
    
        <section id="project">
            <div class="container">
                <div class="project project_list block_list search result">
                    <div class="module">
                        <div class="modulecontent">
                            <ul class="row">
                            <?php while ($row = $listduan->unbuffered_row()) { ?>
                                <li class="col-xs-12 col-sm-6 col-md-4 item aos-init aos-animate" data-aos="fade-up">
                                    <div class="border">
                                        <a class="image" href="?page=bantin&id=<?php echo $row->idbaiviet ?>&idsame=<?php echo $row->id_dm ?>"
                                            style="background-image: url('teamplate/USER/img/<?php echo $row->anhminhhoa ?>')"
                                            title="<?php echo $row->tenbaiviet ?>">
                                            <img src="teamplate/USER/img/<?php echo $row->anhminhhoa ?>">
                                        </a>
                                        <div class="detail">
                                            <h4 class="title"><a href="?page=bantin&id=<?php echo $row->idbaiviet ?>&idsame=<?php echo $row->id_dm ?>"
                                                    title="<?php echo $row->tenbaiviet ?>">
                                                    
                                                    <?php 
                                                    if(strlen(trim($row->tenbaiviet))>30){
                                                    echo substr(trim($row->tenbaiviet),  0, 30)."...";}else{echo $row->tenbaiviet;} ?>
                                                
                                                </a></h4>
                                            <div class="proj"><span>Dự án: </span>
                                            <a href="?page=bantin&id=<?php echo $row->idbaiviet ?>&idsame=<?php echo $row->id_dm ?>" title="<?php echo $row->tenbaiviet ?>"> 
                                            <?php 
                                                    if(strlen(trim($row->tenbaiviet))>30){
                                                    echo substr(trim($row->tenbaiviet),  0, 30)."...";}else{echo $row->tenbaiviet;} ?>
                                                    
                                                </a>
                                            </div>
                                            <div class="blog">
                                               
                                                <dl><dt>Diện tích:</dt>
                                                    <dd><?php echo $row->dientichkhuvuc ?> m2</dd>
                                                </dl>
                                                <dl><dt>Giá:</dt>
                                                    <dd><?php echo number_format($row->gia); ?></dd>
                                                </dl>
                                                <dl><dt>Hướng:</dt>
                                                    <dd><?php echo $row->huongnha ?></dd>
                                                </dl>
                                                <dl><dt>Loại hình:</dt>
                                                
                                                    <dd><?php if($row->idloaitin==2) echo 'Cho Thuê';
                                                     if($row->idloaitin==1) echo 'Tìm Kiếm Và Đầu Tư' ;
                                                     if($row->idloaitin==3) echo 'Rao Bán'; ?>
                                                     </dd>
                                                </dl>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                               
                            </ul>
                        </div>
                        <div class="more">
                            <div class="loading"><img src="http://datxanhbacmientrung.com/frontend/images/loading.gif"></div>
                            <div class="readmore">
                            
                                <a href="?page=duan&id=<?php echo $id; ?>&count=<?php echo $count; ?>">Xem thêm</a></div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
    
        <section class="section newsletter">
            <div id="loading"><img src="http://datxanhbacmientrung.com/frontend/images/loading.gif"></div>
    
        </section>
    
        <script type="text/javascript">
    
            jQuery("#newsletter button.button").click(function () {
    
                var info = $("#info").val();
                var email_regex = /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
                var data_string = 'info=' + info;
    
                if (info == "") {
                    $("#info").addClass('error');
                    $("#info").focus();
                    return false;
                } else {
                    $("#info").removeClass('error');
                }
    
                $("#loading").fadeIn('fast');
                $.ajax({
                    type: "POST",
                    url: url + 'index.php/checkLogin',
                    data: data_string,
                    success: function (data_form) {
                        if (data_form == "true") {
                            $('#loading').fadeOut('fast');
                            $(".notice.success").slideDown('slow').delay(3000).slideUp('slow');
                            clear_form();
                        } else {
                            $('#loading').fadeOut('fast');
                            $(".notice.error").slideDown('slow').delay(3000).slideUp('slow');
                            alert('oke');
                        }
                    } 
                });
                return false;
            });
    
            function clear_form() {
                alert('oke');
                $("#info").val('');
            }
        </script>
        <script type="text/javascript">
            // Button Viewmore featured
            $('.project.result .more a').click(function () {
    
                $(this).parent().prev().fadeIn('fast');
                $('.more .readmore').fadeOut('fast');
    
                var uri = "index.php/checkLogin";
                var num_items = $('#project .block_list .modulecontent .row .item').length;
                var sbProject = "";
                var sbCategory = "";
                var area = "";
                var direction = "";
                var price = "";
    
                var param = new Object();
                param.offset = num_items;
                param.sbProject = sbProject;
                param.sbCategory = sbCategory;
                param.area = area;
                param.direction = direction;
                param.price = price;
    
                $.get(uri, param, function (data) {
                    $('.loading').fadeOut('fast');
                   // document.getElementById('hienthithem').innerHTML= data;
    
                   
                });
            });
    
            $('.bar_search').on('click', function () {
                $('.box_search').slideToggle('slow', function () {
                    if ($('.bar_search span').hasClass('active') == true) {
                        $('.bar_search span').removeClass('active');
                    } else {
                        $('.bar_search span').addClass('active');
                    }
                });
            });
        </script>
    </div>