<div id="main-body">
      
<div class="map" id="map">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d61349.62126471568!2d108.17168633200136!3d16.04724839497301!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x314219c792252a13%3A0xfc14e3a044436487!2zxJDDoCBO4bq1bmcsIEjhuqNpIENow6J1LCDEkMOgIE7hurVuZywgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2sus!4v1543135515281" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

<section class="breadcrumbs">
  <div class="container">
    <div class="content">
      <span class="item"><a href="?page=trangchu" title="Trang chủ">Trang chủ</a></span>
      <span class="item_sep"> / </span>
      <span class="item"><a href="?page=lienhe" title="Liên Hệ">Liên hệ</a></span>
    </div>
  </div>
</section>

<section id="contact">
	<div class="container">
		<div class="contact">
			<!-- <h2 class="title">Liên hệ với chúng tôi</h2> -->
			<div id="contact_inner">
        <div id="sidebar">
          <div class="sidebar__inner">
            <div class="sidebar sb_contact">
	<h5>Liên hệ chúng tôi</h5>
	<div class="contactWrapper">
		<div class="item">
			<div class="title">Văn phòng</div>
			<div class="desc"><i class="fa fa-map-marker"></i><a class="address" href="#">97/06 đường Nguyễn Lương Bằng, phường Hòa Khánh, quận Liên Chiểu, TP. Đà Nẵng</a></div>
		</div>
		
		<div class="item">
			<div class="title">Điện thoại</div>
			<div class="desc"><i class="fa fa-phone"></i><a class="phone" href="tel:02343998999">0975 3579 60</a></div>
		</div>
		<div class="item">
			<div class="title">Di động</div>
			<div class="desc"><i class="fa fa-mobile"></i><a class="phone" href="tel:0964551888">0975 3579 60 - 0916 461 613</a></div>
		</div>
		<div class="item">
			<div class="title">E-mail</div>
			<div class="desc"><i class="fa fa-envelope"></i><a class="mail" href="mailto:lienhe.dxbmt@datxanhmientrung.com">lienhe.dxbmt@datxanhmientrung.com</a></div>
		</div>
	</div>
</div>          </div>
        </div>
				<div class="contact-form">
                    <h2 class="title">Gửi nội dung</h2>
                    <?php if(isset($_GET['thongbao'])) echo '<p><i style="color: red;">'.$_GET['thongbao'].'</i></p>'; ?>
					<form method="post" action="index.php/Home/guiphanhoi" enctype="multipart/form-data">
						<div class="field half first">
							<input type="text" name="hoten" id="fullname" placeholder="Họ tên"/>
                        </div>
						<div class="field half">
							<input type="text" name="email" id="from_email" placeholder="Email"/>
                        </div>
                        <div class="field half first">
							<input type="text" name="tieude" id="fullname" placeholder="Tiêu đề"/>
						</div>
						<div class="field">
							<textarea name="noidung" id="content" rows="10" placeholder="Nội dung"></textarea>
						</div>
						<input type="submit" value="Gửi" class="button submit" >
					</form>
				</div>
				
				<div class="clear"></div>
			</div>
		</div>
	</div>
</section>  


