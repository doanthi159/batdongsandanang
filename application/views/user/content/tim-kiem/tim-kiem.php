<div id="main-body">

    <section class="breadcrumbs">
        <div class="container">
            <div class="content">
                <span class="item"><a href="?page=trangchu" title="Trang chủ">Trang chủ</a></span>
                <span class="item_sep"> / </span>
                <span class="item"><a href="?page=timkiem" title="Kết quả tìm kiếm">Kết quả tìm kiếm</a></span>
            </div>
        </div>
    </section>
            <section class="box_search" style="display:none;">
                <div class="container">

                    <div class="search">
                        <div class="head_title">
                            <div class="title"><span></span>Bất động sản</div>
                        </div>
                        <div class="searchWrapper">
                            <form  method="get"  action="">
                            <input hidden="hidden" name='page' value='find'/>
                                <div class="search-field input-field">
									<label>Loại dự án</label>
									<select name="loaiduan" id="sbCategory">
										<option value="">Chọn loại dự án</option>
										<option value="4">Đất và Móng</option>
										<option value="3">Đất nền</option>
										<option value="2">Biệt thự</option>
										<option value="1">Nhà phố</option>
									</select>
								</div>
								
								
								<div class="search-field input-field">
									<label>Khoảng giá</label>
									<select name="gia">
										<option value=">0">Chọn khoảng giá</option>
										<option value="<1500000000">Dưới 1,5 tỷ</option>
										<option value="BETWEEN 1500000000 and 2500000000">Từ 1,5 - 2,5 tỷ</option>
										<option value="BETWEEN 1500000000 and 2500000000">Từ 2,5 - 3,5 tỷ</option>
										<option value="BETWEEN 3500000000 and 5000000000">Từ 3,5 - 5 tỷ</option>
										<option value="BETWEEN 5000000000 and 7000000000">Từ 5 - 7 tỷ</option>
										<option value="BETWEEN 7000000000 and 10000000000">Từ 7 - 10 tỷ</option>
										<option value=">10000000000">Trên 10 tỷ</option>
									</select>
								</div>
								<div class="search-field input-field">
									<label>Diện tích</label>
									<select name="dientich">
										<option value=">0">Chọn diện tích</option>
										<option value="<100">Dưới 100 m2</option>
										<option value="BETWEEN 100 and 200">Từ 100 - 200 m2</option>
										<option value="BETWEEN 200 and 300">Từ 200 - 300 m2</option>
										<option value="BETWEEN 300 and 500">Từ 300 - 500 m2</option>
										<option value=">500">Trên 500 m2</option>
									</select>
								</div>
                                <div class="search-field input-field">
                                    <label>Tỉnh</label>
                                   <select name="tinh" onchange="gethuyen(this.options[this.selectedIndex].value)">
                                        <option value="">-------Chọn tỉnh thành -------------</option>
                                        <?php while ($row = $tinhthanh->unbuffered_row()) { ?>
                                        <option value="<?php echo $row->matinh ?>"><?php echo $row->ten ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="search-field input-field">
                                    <label>Huyện</label>
                                    <div   id="divhuyen">
                                <select class="form-control" disabled="disabled"  name="huyen" id="huyenoke" >
                                <option value="">-------Chọn quận/huyện -------------</option>
                                </select>
                                </div>
                                </div>
                                <div class="search-field button-field">
                                    <!-- <input type="submit" class="button" value="Tìm kiếm"> -->
                            <button class="button">Tìm kiếm</button>
                        </div>
                    </form>
                </div>
                        </div>
                    </div>
    </section>
                <div class="bar_search"><span>Tìm kiếm</span></div>

                <section id="project">
                    <div class="container">
                        <div class="project project_list block_list search result">
                            <div class="module">
                                <div class="modulecontent">
                                    <ul class="row">

                                        <?php while ($row = $listda->unbuffered_row()) { ?>
                                <li class="col-xs-12 col-sm-6 col-md-4 item aos-init aos-animate" data-aos="fade-up">
                                    <div class="border">
                                        <a class="image" href="?page=bantin&id=<?php echo $row->idbaiviet ?>&idsame=<?php echo $row->id_dm ?>"
                                            style="background-image: url('teamplate/USER/img/<?php echo $row->anhminhhoa ?>')"
                                            title="<?php echo $row->tenbaiviet ?>">
                                            <img src="teamplate/USER/img/<?php echo $row->anhminhhoa ?>">
                                        </a>
                                        <div class="detail">
                                            <h4 class="title"><a href="?page=bantin&id=<?php echo $row->idbaiviet ?>&idsame=<?php echo $row->id_dm ?>"
                                                    title="<?php echo $row->tenbaiviet ?>">
                                                    
                                                    <?php 
                                                    if(strlen(trim($row->tenbaiviet))>30){
                                                    echo substr(trim($row->tenbaiviet),  0, 30)."...";}else{echo $row->tenbaiviet;} ?>
                                                
                                                </a></h4>
                                            <div class="proj"><span>Dự án: </span>
                                            <a href="?page=bantin&id=<?php echo $row->idbaiviet ?>&idsame=<?php echo $row->id_dm ?>" title="<?php echo $row->tenbaiviet ?>"> 
                                            <?php 
                                                    if(strlen(trim($row->tenbaiviet))>30){
                                                    echo substr(trim($row->tenbaiviet),  0, 30)."...";}else{echo $row->tenbaiviet;} ?>
                                                    
                                                </a>
                                            </div>
                                            <div class="blog">
                                               
                                                <dl><dt>Diện tích:</dt>
                                                    <dd><?php echo $row->dientichkhuvuc ?> m2</dd>
                                                </dl>
                                                <dl><dt>Giá:</dt>
                                                    <dd><?php echo number_format($row->gia); ?></dd>
                                                </dl>
                                                <dl><dt>Hướng:</dt>
                                                    <dd><?php echo $row->huongnha ?></dd>
                                                </dl>
                                                <dl><dt>Loại hình:</dt>
                                                
                                                    <dd><?php if($row->idloaitin==2) echo 'Cho Thuê';
                                                     if($row->idloaitin==1) echo 'Tìm Kiếm Và Đầu Tư' ;
                                                     if($row->idloaitin==3) echo 'Rao Bán'; ?>
                                                     </dd>
                                                </dl>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                                            <div id='hienthithem'></div>
                           
                        </ul>
                    </div>
                                    <div class="more">
                                        <div class="loading"><img src="http://datxanhbacmientrung.com/frontend/images/loading.gif"></div>
                                            <div class="readmore"><a href="javascript:void(0);">Xem thêm</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
    </section>


                        <section class="section newsletter">
                            <div id="loading"><img src="http://datxanhbacmientrung.com/frontend/images/loading.gif"></div>

    </section>

                            <script type="text/javascript">

                                jQuery("#newsletter button.button").click(function () {

            var info = $("#info").val();
            var email_regex = /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
                                var data_string = 'info=' + info;
                    
            if (info == "") {
                                    $("#info").addClass('error');
                                $("#info").focus();
                                return false;
            } else {
                                    $("#info").removeClass('error');
                                }
                    
                                $("#loading").fadeIn('fast');
            $.ajax({
                                    type: "POST",
                                url: url + 'index.php/checkLogin',
                                data: data_string,
                success: function (data_form) {
                    if (data_form == "true") {
                                    $('#loading').fadeOut('fast');
                                $(".notice.success").slideDown('slow').delay(3000).slideUp('slow');
                                clear_form();
                    } else {
                                    $('#loading').fadeOut('fast');
                                $(".notice.error").slideDown('slow').delay(3000).slideUp('slow');
                                alert('oke');
                            }
                        }
                    });
                    return false;
                });
        
        function clear_form() {
                                    alert('oke');
            $("#info").val('');
                            }
    </script>
    <script type="text/javascript">
                            // Button Viewmore featured
        $('.project.result .more a').click(function () {

                                    $(this).parent().prev().fadeIn('fast');
                                $('.more .readmore').fadeOut('fast');
                    
                                var uri = "index.php/checkLogin";
                                var num_items = $('#project .block_list .modulecontent .row .item').length;
                                var sbProject = "";
                                var sbCategory = "";
                                var area = "";
                                var direction = "";
                                var price = "";
                    
                                var param = new Object();
                                param.offset = num_items;
                                param.sbProject = sbProject;
                                param.sbCategory = sbCategory;
                                param.area = area;
                                param.direction = direction;
                                param.price = price;
                    
            $.get(uri, param, function (data) {
                                    $('.loading').fadeOut('fast');
                                // document.getElementById('hienthithem').innerHTML= data;


                                });
                            });
                    
        $('.bar_search').on('click', function () {
                                    $('.box_search').slideToggle('slow', function () {
                                        if ($('.bar_search span').hasClass('active') == true) {
                                            $('.bar_search span').removeClass('active');
                                        } else {
                                            $('.bar_search span').addClass('active');
                                        }
                                    });
        });
    </script>
</div>