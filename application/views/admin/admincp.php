<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
 	
		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		
		<title>Quản trị viên</title>

		<link rel="stylesheet" href="../teamplate/ADMIN/resources/css/reset.css" type="text/css" media="screen" />
	  
		<!-- Main Stylesheet -->
		<link rel="stylesheet" href="../teamplate/ADMIN/resources/css/style.css" type="text/css" media="screen" />
		
		<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
		<link rel="stylesheet" href="../teamplate/ADMIN/resources/css/invalid.css" type="text/css" media="screen" />
		<script src="../teamplate/ADMIN/ckeditor/ckeditor.js"></script>	
		
	                    -->
  
		<!-- jQuery -->
		<script type="text/javascript" src="../teamplate/ADMIN/resources/scripts/jquery-1.3.2.min.js"></script>
		
		<!-- jQuery Configuration -->
		<script type="text/javascript" src="../teamplate/ADMIN/resources/scripts/simpla.jquery.configuration.js"></script>
		
		<!-- Facebox jQuery Plugin -->
		<script type="text/javascript" src="../teamplate/ADMIN/resources/scripts/facebox.js"></script>
		
		<!-- jQuery WYSIWYG Plugin -->
		<script type="text/javascript" src="../teamplate/ADMIN/resources/scripts/jquery.wysiwyg.js"></script>
	
		<script type="text/javascript" src="../teamplate/ADMIN/resources/scripts/jquery.datePicker.js"></script>
		<script type="text/javascript" src="../teamplate/ADMIN/resources/scripts/jquery.date.js"></script>

  <link href="../teamplate/USER/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../teamplate/USER/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="../teamplate/USER/bootstrap/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="../teamplate/USER/bootstrap/css/sb-admin.css" rel="stylesheet">
	</head>
  
	<body>
		<div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->
		
		<div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->
			
			<h1 id="sidebar-title"><a href="#">ADMIN PANEL</a></h1>
		  
			<!-- Logo (221px wide) -->
			<a href="#"><img id="logo" src="../teamplate/ADMIN/resources/images/logo.png" alt="Simpla Admin logo" /></a>
		  
			<!-- Sidebar Profile links -->
			<div id="profile-links">
				Xin chào, <a href="#" title="Edit your profile">admin</a>, đã đăng nhập <a href="#messages" rel="modal" title="3 Messages">hệ thống</a><br />
				<br />
				<a href="" title="View the Site">Xem web site</a> | <a href="admin/logout" title="Sign Out">Đăng xuất</a>
			</div>        
			
			<ul id="main-nav">  <!-- Accordion Menu -->
				
				<li>
					<a href="" class="nav-top-item current"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
						Quản lý bài đăng
					</a>  

					<ul>
						<li><a href="?page=baiviet">Danh sách bài đăng</a></li>
						
					</ul>     
				</li>
				<li>
					<a href="" class="nav-top-item"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
						Quản lý tin tức
					</a>  

					<ul>
						<li><a href="?page=tintuc&id=1">Tin tức BDS Đà Nẵng </a></li>
						<li><a href="?page=tintuc&id=2">Tin tức thị trường </a></li>
					</ul>     
				</li>
				
				
				
				<li>
					<a href="#" class="nav-top-item">
						Quản lý danh mục
					</a>
					<ul>
						<li><a href="?page=danhmuc">Tất cả danh mục</a></li>
					
						
					</ul>
				</li>

				<li>
					<a href="#" class="nav-top-item">
					Album
					</a>
					<ul>
						<li><a href="?page=pig">Danh sách hình ảnh</a></li>
					
					</ul>
				</li>
				<li>
					<a href="#" class="nav-top-item">
					Phản hồi& Liên hệ
					</a>
					<ul>
						<li><a href="?page=phanhoi">Danh sách phản hồi</a></li>
					</ul>
				</li>
				
					<li>
					<a href="#" class="nav-top-item">
					Hệ thống
					</a>
					<ul>
						<li><a href="?page=slide">Chỉnh sửa slide</a></li>
					</ul>
				</li>


			





				
				
				
			</ul> <!-- End #main-nav -->
			
			<div id="messages" style="display: none"> <!-- Messages are shown when a link with these attributes are clicked: href="#messages" rel="modal"  -->
				
				<h3>3 Messages</h3>
			 
				<p>
					<strong>17th May 2009</strong> by Admin<br />
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
					<small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
				</p>
			 
				<p>
					<strong>2nd May 2009</strong> by Jane Doe<br />
					Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.
					<small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
				</p>
			 
				<p>
					<strong>25th April 2009</strong> by Admin<br />
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
					<small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
				</p>
				
				<form action="#" method="post">
					
					<h4>New Message</h4>
					
					<fieldset>
						<textarea class="textarea" name="textfield" cols="79" rows="5"></textarea>
					</fieldset>
					
					<fieldset>
					
						<select name="dropdown" class="small-input">
							<option value="option1">Send to...</option>
							<option value="option2">Everyone</option>
							<option value="option3">Admin</option>
							<option value="option4">Jane Doe</option>
						</select>
						
						<input class="button" type="submit" value="Send" />
						
					</fieldset>
					
				</form>
				
			</div> <!-- End #messages -->
			
		</div></div> <!-- End #sidebar -->
		
		<div id="main-content"> <!-- Main Content Section with everything -->
			
			<noscript> <!-- Show a notification if the user has disabled javascript -->
				<div class="notification error png_bg">
					<div>
						Javascript is disabled or is not supported by your browser. Please <a href="http://browsehappy.com/" title="Upgrade to a better browser">upgrade</a> your browser or <a href="http://www.google.com/support/bin/answer.py?answer=23852" title="Enable Javascript in your browser">enable</a> Javascript to navigate the interface properly.
					Download From <a href="http://www.exet.tk">exet.tk</a></div>
				</div>
			</noscript>
			
			<!-- Page Head -->
			<h2>Chào mừng bạn đến với trang quản trị</h2>
			<p id="page-intro">Chúc bạn một ngày làm việc thật vui vẻ.</p>
			<?php 
				if(isset($_GET['thongbao'])){
			?>
			<div class="notification success png_bg">
				<a href="#" class="close"><img src="../teamplate/ADMIN/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close"></a>
				<div>
					<?php echo $_GET['thongbao']; ?>
				</div>
			</div>
			<?php } ?>

			<?php 
				if(isset($_GET['error'])){
			?>
			<div class="notification error png_bg">
				<a href="#" class="close"><img src="../teamplate/ADMIN/resources/images/icons/cross_grey_small.png" title="Close this notification" alt="close"></a>
				<div>
					<?php echo $_GET['error']; ?>
				</div>
			</div>
			<?php } ?>

			<div class="clear"></div> <!-- End .clear -->
			








<?php $this->load->view($page); ?>


		
			<div class="clear"></div>
			
			
			<!-- Start Notifications -->
			
			
			
			<div id="footer">
				<small> <!-- Remove this notice or replace it with whatever you want -->
						&#169; Copyright 2009 Your Company | Powered by <a href="http://themeforest.net/item/simpla-admin-flexible-user-friendly-admin-skin/46073">Simpla Admin</a> | <a href="#">Top</a>
				</small>
			</div><!-- End #footer -->
			
		</div> <!-- End #main-content -->
		
	</div></body>
  

<!-- Download From www.exet.tk-->
</html>


