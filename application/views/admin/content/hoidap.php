<style>
	
	.page-link {
    position: relative;
    display: block;
    padding: .5rem .75rem;
    margin-left: -1px;
    line-height: 1.25;
    color: #469400;
    background-color: #fff;
    border: 1px solid #ddd;


}
.pagination {
    display: -ms-flexbox;
    display: flex;
    padding-left: 0;
    list-style: none;
    border-radius: .25rem;
    margin-left: 70%;
}

.page-item.active .page-link {
    z-index: 2;
    color: #fff;
    background-color: #469400;
    border-color: #469400;
}
.paginate_button {
list-style:none;
height: 30px;


}
#dataTable_filter{
	float: right;

}
#dataTable_filter input{
	    margin-left: 0.5em;
    display: inline-block;
    width: auto;
	
}
.form-control-sm{
	height: 30px;
}

</style>
				

			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>Kiểm duyệt bài đăng</h3>
					
					<ul class="content-box-tabs">
						<li><a href="#tab1" class="default-tab">Danh sách</a></li> <!-- href must be unique and match the id of target div -->
						
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
						
					
						
						<table class="table table-bordered" id="dataTable">
							
							<thead>
								<tr>
								  
								   <th>Người hỏi</th>
								   <th>Thời gian</th>
								   <th>Tiêu đề</th>
								   <th>Nội dung</th>
								   <th>Đã trả lời</th>
								   <th>Chức năng</th>
								</tr>
								
							</thead>
						 
					
						 
							<tbody>
								<?php while ($row = $ch->unbuffered_row()) { ?>
								<tr>
								
									<td width="10%"><?php echo $row->ho_ten; ?></td>
									<td width="10%"><?php echo $row->thoi_gian; ?></td>
									<td width="15%"><?php echo $row->tieu_de; ?></td>
									<td width="45%"><?php echo $row->noi_dung; ?></td>
									<?php $count=$this->db->query("sELECT count(*) as tong from phanhoi_cauhoi where id_cauhoi=".$row->id_cauhoi);
									while ($row2 = $count->unbuffered_row()) {
										echo '<td width="10%">'.$row2->tong.' </td>';
									}
									 ?>
									
									<td width="5%">
										<!-- Icons -->
										
										 <a href="?page=xemhoidap&id=<?php echo $row->id_cauhoi ?>" title="Xem và Phản hồi"><img src="../teamplate/ADMIN/resources/images/icons/view.png" alt="Phản hồi" /></a>
									</td>
								</tr>
								<?php } ?>
								
							</tbody>
							
						</table>
						
					</div> <!-- End #tab1 -->
					
				        
					
				</div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->

	 <script src="../teamplate/USER/bootstrap/vendor/jquery/jquery.min.js"></script>
    <script src="../teamplate/USER/bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../teamplate/USER/bootstrap/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../teamplate/USER/bootstrap/vendor/datatables/jquery.dataTables.js"></script>
    <script src="../teamplate/USER/bootstrap/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../teamplate/USER/bootstrap/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../teamplate/USER/bootstrap/js/sb-admin-datatables.min.js"></script>