<style>
	
	.page-link {
    position: relative;
    display: block;
    padding: .5rem .75rem;
    margin-left: -1px;
    line-height: 1.25;
    color: #469400;
    background-color: #fff;
    border: 1px solid #ddd;


}
.pagination {
    display: -ms-flexbox;
    display: flex;
    padding-left: 0;
    list-style: none;
    border-radius: .25rem;
    margin-left: 70%;
}

.page-item.active .page-link {
    z-index: 2;
    color: #fff;
    background-color: #469400;
    border-color: #469400;
}
.paginate_button {
list-style:none;
height: 30px;


}
#dataTable_filter{
	float: right;

}
#dataTable_filter input{
	    margin-left: 0.5em;
    display: inline-block;
    width: auto;
	
}
.form-control-sm{
	height: 30px;
}

</style>
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>Quản lý slide</h3>
					
					<ul class="content-box-tabs">
						<li><a href="#tab1" class="default-tab">Danh sách</a></li> <!-- href must be unique and match the id of target div -->
						<li><a href="#tab2">Thêm mới </a></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
						
					
						
						<table class="table table-bordered" id="dataTable">
							
							<thead>
								<tr>
								   <th><input class="check-all" type="checkbox" /></th>
								   <th>id</th>
								   <th>img</th>
								   <th>Chức năng</th>
								</tr>
								
							</thead>
						 
						
						 
							<tbody>








<?php 

while ($row = $slide->unbuffered_row())
{

	?>

								<tr>
									<td><input type="checkbox" /></td>
									<td><?php echo $row->id ?></td>
									<td><img src="../teamplate/USER/img/<?php echo $row->src ?>" width="100px;" height="100px" /></td>
									
									<td>
										<!-- Icons -->
										
										 <a href="admin/delete_slide?id=<?php echo $row->id ?>" title="Delete"><img src="../teamplate/ADMIN/resources/images/icons/cross.png" alt="Delete" onclick="return confirm('Bạn có chắc xóa không?');" /></a> 
										
									</td>
								</tr>
								

<?php } ?>











								
							</tbody>
							
						</table>
						
					</div> <!-- End #tab1 -->
					
					<div class="tab-content" id="tab2">
					
						<form action="admin/add_slide" method="post" enctype="multipart/form-data">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->

					
								<p>
									<label>Chọn ảnh muốn thêm</label>
										<input class="text-input small-input" type="file" id="small-input" name="image" /> 
									
								</p>

								
								<p>
									<input class="button" type="submit" value="Thực hiện thêm" />
								</p>
								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
						
					</div> <!-- End #tab2 -->        
					
				</div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->

	 <script src="../teamplate/USER/bootstrap/vendor/jquery/jquery.min.js"></script>
    <script src="../teamplate/USER/bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../teamplate/USER/bootstrap/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../teamplate/USER/bootstrap/vendor/datatables/jquery.dataTables.js"></script>
    <script src="../teamplate/USER/bootstrap/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../teamplate/USER/bootstrap/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../teamplate/USER/bootstrap/js/sb-admin-datatables.min.js"></script>