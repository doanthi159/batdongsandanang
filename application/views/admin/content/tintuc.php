  <link href="../teamplate/USER/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../teamplate/USER/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="../teamplate/USER/bootstrap/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="../teamplate/USER/bootstrap/css/sb-admin.css" rel="stylesheet">
  <style>
	
	.page-link {
    position: relative;
    display: block;
    padding: .5rem .75rem;
    margin-left: -1px;
    line-height: 1.25;
    color: #469400;
    background-color: #fff;
    border: 1px solid #ddd;


}
.pagination {
    display: -ms-flexbox;
    display: flex;
    padding-left: 0;
    list-style: none;
    border-radius: .25rem;
    margin-left: 70%;
}

.page-item.active .page-link {
    z-index: 2;
    color: #fff;
    background-color: #469400;
    border-color: #469400;
}
.paginate_button {
list-style:none;
height: 30px;


}
#dataTable_filter{
	float: right;

}
#dataTable_filter input{
	    margin-left: 0.5em;
    display: inline-block;
    width: auto;
	
}
.form-control-sm{
	height: 30px;
}

</style>


			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>Quản lý tin tức </h3>
					
					<ul class="content-box-tabs">
						<li><a href="#tab1" class="default-tab">Danh sách</a></li> <!-- href must be unique and match the id of target div -->
						<li><a href="#tab2">Thêm mới </a></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content ">
					
					<div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
						
					
						
						<table class="table table-bordered" id="dataTable">
							
							<thead>
								<tr>
								   <th><input class="check-all" type="checkbox" /></th>
								   <th>ID</th>
								   <th>ID loại tin</th>
								   <th>Ảnh minh họa</th>
								   <th>Tên bài viết</th>
								   <th>Nội dung tóm tắt</th>
								    <th>Chức năng</th>
								   
								</tr>
								
							</thead>
						 
							
						 
							<tbody>




<?php while ($row = $bv->unbuffered_row()) { ?>

								<tr>
									<td width="3%"><input type="checkbox" /></td>
									<td width="3%"><?php echo $row->idtin ?></td>
									<td width="9%"><?php echo $row->idtintuc ?></td>
									<td width="15%"><img src="../teamplate/USER/img/<?php echo $row->anhminhhoa ?>" style="width:50px;height: 50px" /></td>
									<td width="20%"><?php echo $row->tenbaiviet ?></td>
									<td width="40%">D<?php echo $row->tomtat ?></td>
									<td width="10%">
										<!-- Icons -->
										 <a href="?page=tintucsua&id=<?php echo $row->idtin ?>" title="Edit"><img src="../teamplate/ADMIN/resources/images/icons/pencil.png" alt="Edit" /></a>
										 <a href="admin/delete_tintuc?id=<?php echo $row->idtin ?>" title="Delete"><img src="../teamplate/ADMIN/resources/images/icons/cross.png" alt="Delete" onclick="return confirm('Bạn có chắc xóa không?');"/></a> 
									</td>
								</tr>
								

<?php } ?>


							</tbody>
							
						</table>
						
					</div> <!-- End #tab1 -->
					
					<div class="tab-content" id="tab2">
					
						<form action="admin/add_tintuc?id=<?php echo $idtin ?>" method="post" enctype="multipart/form-data">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
<p>
									<label>Loại tin</label>              
									<select name="idloaitin" class="small-input">
										<option value="1">BĐS Đà Nẵng</option>
										<option value="2">Thị trường</option>
									</select> 
								</p>
								


					
								<p>
									<label>Ảnh minh họa</label>
										<input class="text-input small-input" type="file" id="small-input" name="image" required/> 
									
								</p>

								


 								<p>
										<label>Tiêu đề bài viết</label>     
									<div class="form-group">
										<textarea class="form-control" rows="5" name="tieude" required></textarea>
									
									</div>

								</p>

							    <p>
										<label>Tóm tắt ngắn</label>     
									<div class="form-group">
										<textarea class="form-control" rows="5" name="tomtat" required></textarea>
										
									</div>

								</p>
								
								<p>
										<label>Nội dung</label>     
									<div class="form-group">
										<textarea class="form-control" rows="5" name="noidung" required></textarea>
										<script type="text/javascript">
											CKEDITOR.replace('noidung');
										</script>
									</div>

								</p>
								
								<p>
									<input class="button" type="submit" value="Thực hiện" />
								</p>
								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
						
					</div> <!-- End #tab2 -->        
					
				</div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->

	 <script src="../teamplate/USER/bootstrap/vendor/jquery/jquery.min.js"></script>
    <script src="../teamplate/USER/bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../teamplate/USER/bootstrap/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../teamplate/USER/bootstrap/vendor/datatables/jquery.dataTables.js"></script>
    <script src="../teamplate/USER/bootstrap/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../teamplate/USER/bootstrap/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../teamplate/USER/bootstrap/js/sb-admin-datatables.min.js"></script>