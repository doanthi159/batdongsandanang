		<style>
	
	.page-link {
    position: relative;
    display: block;
    padding: .5rem .75rem;
    margin-left: -1px;
    line-height: 1.25;
    color: #469400;
    background-color: #fff;
    border: 1px solid #ddd;


}
.pagination {
    display: -ms-flexbox;
    display: flex;
    padding-left: 0;
    list-style: none;
    border-radius: .25rem;
margin-left: 70%;
}

.page-item.active .page-link {
    z-index: 2;
    color: #fff;
    background-color: #469400;
    border-color: #469400;
}
.paginate_button {
list-style:none;
height: 30px;


}
#dataTable_filter{
	float: right;

}
#dataTable_filter input{
	    margin-left: 0.5em;
    display: inline-block;
    width: auto;
	
}
.form-control-sm{
	height: 30px;
}

</style>


			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>Quản lý thành viên</h3>
					
					<ul class="content-box-tabs">
						<li><a href="#tab1" class="default-tab">Danh sách</a></li> <!-- href must be unique and match the id of target div -->
						<li><a href="#tab2">Thêm mới </a></li>
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
						
					
						
						<table class="table table-bordered" id="dataTable">
							
							<thead>
								<tr>
								   <th>ID</th>
								    <th>Mã SV</th>
								    <th>Họ tên</th>
								    <th>Ngày sinh</th>
								    <th>Giới Tính</th>
								   <th>Khóa</th>
								   <th>Lớp</th>
								  
								    <th>Chức năng</th>
								</tr>
								
							</thead>
						 
							
							<tbody>
								<?php while ($row = $sv->unbuffered_row()) { ?>
								<tr>
									<td><?php echo $row->id_sv ?></td>
									<td><?php echo $row->ma_sv ?></td>
									<td><?php echo $row->ho_ten ?></td>
									<td><?php echo $row->ngay_sinh ?></td>
									<td><?php echo $row->gioi_tinh ?></td>
									<td><?php echo $row->nien_khoa ?></td>
									<td><?php echo $row->ten_lop ?></td>
								
									<td>
										<!-- Icons -->
										 <a href="?page=thanhviensua&ma_sv=<?php echo $row->ma_sv ?>" title="Edit"><img src="../teamplate/ADMIN/resources/images/icons/pencil.png" alt="Edit" /></a>
										 <a href="admin/delete_sv?id=<?php echo $row->ma_sv ?>" title="Delete"  onclick="return confirm('Bạn có chắc xóa không?');" ><img src="../teamplate/ADMIN/resources/images/icons/cross.png" alt="Delete" /></a> 
										
									</td>
								</tr>
								
							
							<?php } ?>
							</tbody>
							
						</table>
						
					</div> <!-- End #tab1 -->
					
					<div class="tab-content" id="tab2">
					
						<form action="admin/add_sv" method="post" enctype="multipart/form-data">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
						  <p>
									<label>Lớp</label>              
									<select name="malop" class="small-input">
										<?php while ($row = $lop->unbuffered_row()) { ?>
										<option value="<?php echo $row->ma_lop ?>"><?php echo $row->ten_lop; ?></option>
										
										<?php } ?>
										
									</select> 
								</p>
														
					
								<p>
									<label>Họ tên</label>
										<input class="text-input small-input" type="text" id="small-input" name="ho_ten" required="" /> 
									
								</p>
									<p>
									<label>Mã sinh viên</label>
										<input class="text-input small-input" type="text" id="small-input" name="ma_sv" required="" /> 
									
								</p>
								<p>
									<label>Mật khẩu</label>
										<input class="text-input small-input" type="password" id="small-input" name="mat_khau" required="" /> 
									
								</p>
									<p>
									<label>Ngày sinh</label>
										<input class="text-input small-input" type="password" id="small-input" name="ngay_sinh" required="" /> 
									
								</p>
								<p>
									<label>Giới tính</label>
										<input  type="radio" id="small-input" name="gioi_tinh" value="Nam" checked="checked" /> Nam 
										<input  type="radio" id="small-input" name="gioi_tinh" value="Nữ" /> Nữ
									
								</p>

								 <p>
									<label>Khóa</label>              
									<select name="nien_khoa" class="small-input">
										<option value="2013">2013</option>
										<option value="2014">2014</option>
										<option value="2015">2015</option>
										<option value="2016">2016</option>
										<option value="2017">2017</option>
										<option value="2018">2018</option>
										<option value="2019">2019</option>
										<option value="2020">2020</option>
										<option value="2021">2021</option>
										<option value="2022">2022</option>
										<option value="2023">2023</option>
										<option value="2016">2016</option>
										<option value="2024">2024</option>
										<option value="2025">2025</option>
										<option value="2026">2026</option>
										<option value="2027">2027</option>
										<option value="2028">2028</option>
										<option value="2028">2028</option>
										
										
									</select> 
								</p>

								
								<p>
									<input class="button" type="submit" value="Thực hiện thêm" />
								</p>
								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
						
					</div> <!-- End #tab2 -->        
					
				</div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->

	 <script src="../teamplate/USER/bootstrap/vendor/jquery/jquery.min.js"></script>
    <script src="../teamplate/USER/bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../teamplate/USER/bootstrap/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../teamplate/USER/bootstrap/vendor/datatables/jquery.dataTables.js"></script>
    <script src="../teamplate/USER/bootstrap/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../teamplate/USER/bootstrap/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../teamplate/USER/bootstrap/js/sb-admin-datatables.min.js"></script>