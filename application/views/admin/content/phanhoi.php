			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>Kiểm duyệt bài đăng</h3>
					
					<ul class="content-box-tabs">
						<li><a href="#tab1" class="default-tab">Danh sách</a></li> <!-- href must be unique and match the id of target div -->
						
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
						
					
						
						<table>
							
							<thead>
								<tr>
								 
								   <th>Người đăng</th>
								    <th>Email</th>
								   <th>Thời gian</th>
								   <th>Tiêu đề</th>
								   <th>Nội dung</th>
								   <th>Chức năng</th>
								</tr>
								
							</thead>
						 
							
						 
							<tbody>
								<?php while ($row = $ph->unbuffered_row()) { ?>
								<tr>
								    <td><?php echo $row->hoten; ?></td>
									<td><?php echo $row->email; ?></td>
									<td><?php echo $row->thoi_gian; ?></td>
									<td><?php echo $row->tieude; ?> </td>
									<td><?php echo $row->noidung; ?></td>
									<td>
										<!-- Icons -->
										 <a href="?page=xemphanhoi&id=<?php echo $row->id_ph ?>" title="Xem"><img src="../teamplate/ADMIN/resources/images/icons/view.png" alt="Xem" /></a>
										
									</td>
								</tr>
								<?php } ?>
								
							</tbody>
							
						</table>
						
					</div> <!-- End #tab1 -->
					
				        
					
				</div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->