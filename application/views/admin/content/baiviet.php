  <link href="../teamplate/USER/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../teamplate/USER/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="../teamplate/USER/bootstrap/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="../teamplate/USER/bootstrap/css/sb-admin.css" rel="stylesheet">
  <style>
	
	.page-link {
    position: relative;
    display: block;
    padding: .5rem .75rem;
    margin-left: -1px;
    line-height: 1.25;
    color: #469400;
    background-color: #fff;
    border: 1px solid #ddd;


}
.pagination {
    display: -ms-flexbox;
    display: flex;
    padding-left: 0;
    list-style: none;
    border-radius: .25rem;
    margin-left: 70%;
}

.page-item.active .page-link {
    z-index: 2;
    color: #fff;
    background-color: #469400;
    border-color: #469400;
}
.paginate_button {
list-style:none;
height: 30px;


}
#dataTable_filter{
	float: right;

}
#dataTable_filter input{
	    margin-left: 0.5em;
    display: inline-block;
    width: auto;
	
}
.form-control-sm{
	height: 30px;
}

</style>


			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>Quản lý bài đăng </h3>
					
					<ul class="content-box-tabs">
						<li><a href="#tab1" class="default-tab">Danh sách</a></li> <!-- href must be unique and match the id of target div -->
					</ul>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content ">
					
					<div class="tab-content default-tab" id="tab1"> <!-- This is the target div. id must match the href of this div's tab -->
						
					
						
						<table class="table table-bordered" id="dataTable">
							
							<thead>
								<tr>
								   <th><input class="check-all" type="checkbox" /></th>
								   <th>ID</th>
								   <th>ID loại tin</th>
								   <th>Ảnh minh họa</th>
								   <th>Tên bài viết</th>
								   <th>Nội dung tóm tắt</th>
								   <th>Chức năng</th>
								   
								</tr>
								
							</thead>
						 
							
						 
							<tbody>




<?php while ($row = $bv->unbuffered_row()) { ?>

								<tr>
									<td width="3%"><input type="checkbox" /></td>
									<td width="3%"><?php echo $row->idbaiviet ?></td>
									<td width="9%"><?php echo $row->idloaitin ?></td>
									<td width="15%"><img src="../teamplate/USER/img/<?php echo $row->anhminhhoa ?>" style="width:50px;height: 50px" /></td>
									<td width="20%"><?php echo $row->tenbaiviet ?></td>
									<td width="40%">D<?php echo $row->tomtat ?></td>
									<td width="10%">
										<!-- Icons -->
										<?php if($row->trangthai==0){ ?>
										<a href="?page=baivietsua&id=<?php echo $row->idbaiviet ?>" title="Phê duyệt"><img style="width:23px;height:23px;" src="../teamplate/ADMIN/resources/images/icons/verify.png" alt="Edit" /></a>
										<?php }else{ ?>
											<img style="width:23px;height:23px;" title="Đã phê duyệt" src="../teamplate/ADMIN/resources/images/icons/verifyd.png" alt="Edit" disabled />
											<?php } ?>
										 <a href="admin/delete_bv?id=<?php echo $row->idbaiviet ?>" title="Delete"><img src="../teamplate/ADMIN/resources/images/icons/cross.png" alt="Delete" onclick="return confirm('Bạn có chắc xóa không?');"/></a> 
										
									</td>
								</tr>
								

<?php } ?>


							</tbody>
							
						</table>
						
					</div> <!-- End #tab1 -->
				
					
				</div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->

	 <script src="../teamplate/USER/bootstrap/vendor/jquery/jquery.min.js"></script>
    <script src="../teamplate/USER/bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../teamplate/USER/bootstrap/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../teamplate/USER/bootstrap/vendor/datatables/jquery.dataTables.js"></script>
    <script src="../teamplate/USER/bootstrap/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../teamplate/USER/bootstrap/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../teamplate/USER/bootstrap/js/sb-admin-datatables.min.js"></script>