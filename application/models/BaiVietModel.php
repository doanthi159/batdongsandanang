<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BaiVietModel extends CI_Model 
{

	public $fillable = array();

	public $protected = array();

	public function __construct() {

		parent::__construct();

		$this->table              = 'baiviet';

	}
	public function getBaiVietTheoId($id)
	{
        $this->load->database();

         $this->db->where("id_kh", $id);

        $query =   $this->db->get($this->table)->result_array();

        return $query;

	}
	public function deleteBaiVietTheoId($id)
	{
        $this->load->database();

         $this->db->where("id_kh", $id);

        $query =   $this->db->delete($this->table);

        return $query;

	}

}