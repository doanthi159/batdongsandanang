<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['translate_uri_dashes'] = FALSE;

$route['default_controller'] = 'Home';

$route['login'] = 'Home/userLogin';
$route['dangtin'] = 'Home/add_tin';
$route['userLogout'] = 'Home/userLogout';
$route['register'] = 'Home/register';

$route['checkLogin'] = 'Home/checkLogin';
$route['admin'] = 'Admin';

$route['test$'] = 'testdb';

$route['phantrang.html'] = 'phantrang';
$route['phantrang/:num'] = 'phantrang/index/$1'; 


$route['404_override'] = '';


